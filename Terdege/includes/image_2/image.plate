{% comment %}File location: 'includes/image/image.plate'{% endcomment %}
{%- include 'includes/image/image_array' -%}

{% comment %}Assign prefix for when you want to use lazyload{% endcomment %}
{% assign dataPrefixClass = '' %}
{%- if imageLazyload and site.lazyload and lazyloadFilter != true -%}
	{% assign dataPrefixClass = 'class="lazy" data-'%}
{%- endif -%}
{%- if imageLazyload == 'swiper' and site.lazyload -%}
	{% assign dataPrefixClass = 'class="swiper-lazy" data-'%}
{%- endif -%}

{% comment %}If alt text is not defined, an alt text is assembled from the filename{% endcomment %}
{%- if imageAlt == '' -%}
	{% assign newAlt = imageSource.file_uid | split: '.' | first | replace: '-', ' ' | replace: '_', ' ' | remove: '1' | remove: '2' | remove: '3' | remove: '4' | remove: '5' | remove: '6' | remove: '7' | remove: '8' | remove: '9' | remove: '10'  %}
{%- else -%}
	{% assign newAlt = imageAlt %}
{%- endif -%}

{% capture imageCapture %}
{%- if imageSource.src contains '.svg' -%}
{% comment %}SVG images, the sizes will be applied by css. Svgs are vectors, so minifying is not needed{% endcomment %}
	<img id="image-{{element.id}}{{section.id}}{{post.id}}" src="{{ imageSource | img_url }}" alt="{{newAlt}}" class="{{imageClass}}">
	<style>
		#image-{{element.id}}{{section.id}}{{post.id}}{
			{%- if imageBreakpoints[0][3] and imageBreakpoints[0][3] != 0 and imageBreakpoints[0][3] != '0'   -%}
				width: {{imageBreakpoints[0][3]}};
				{%- unless imageBreakpoints[0][3] contains 'px' or imageBreakpoints[0][3] contains '%' -%}
					width: {{ 100 | divided_by: 12.0 | times: imageBreakpoints[0][3] | round}}%;
				{%- endunless -%}
			{%- endif -%}
			{%- if imageBreakpoints[0][4] and imageBreakpoints[0][4] != 0 and imageBreakpoints[0][4] != '0'  -%}
				height: {{imageBreakpoints[0][4]}};
			{%- endif -%}
		}
		{%- for breakpoint in imageBreakpoints -%}
			{%- unless forloop.first -%}
			@media only screen and (min-width: {{breakpoint[1]}}px){
						#image-{{element.id}}{{section.id}}{{post.id}}{
							{%- if breakpoint[3] and breakpoint[3] != 0 and breakpoint[3] != '0'   -%}
								width: {{breakpoint[3]}};
								{%- unless breakpoint[3] contains 'px' or breakpoint[3] contains '%' -%}
									width: {{ 100 | divided_by: 12.0 | times: breakpoint[3] | round}}%;
								{%- endunless -%}
							{%- endif -%}
							{%- if breakpoint[4] and breakpoint[4] != 0 and breakpoint[4] != '0'  -%}
								height: {{breakpoint[4]}};
							{%- endif -%}
						 }
				 }
			{%- endunless -%}
		{%- endfor -%}
	</style>
{%- else -%}
<picture class="{% if backgroundMode == true  -%}background__img{%- endif %} {{imageClass}}">

	{% comment %}Get sources for the different viewports and formats{% endcomment %}
	{%- include 'includes/image/image_loop', format: 'webp' -%}
	{%- if imagePng -%}
		{%- include 'includes/image/image_loop', format: 'png' -%}
	{%- else -%}
		{%- include 'includes/image/image_loop', format: 'jpeg' -%}
	{%- endif -%}

	{% comment %}define the backup image, this is also the mobile variant{% endcomment %}
	{%- assign backupImgColumnWidth = arrayViewports | first | split: '-' | first -%}
	{%- assign backupImgWidth = imageBreakpoints[0][3] -%}
	{%- assign backupImgHeight = imageBreakpoints[0][4] -%}
	<source type="image/webp" media="(max-width: {{-imageBreakpoints[1][0] | minus: 1-}}px)" {{-dataPrefixClass-}}srcset="{%- include 'includes/image/image_url', imgWidth: backupImgWidth, imgHeight: backupImgHeight, imgColumnWidth: backupImgColumnWidth, format: 'webp' -%}">
	{%- if imagePng -%}
		<source type="image/png" media="(max-width: {{-imageBreakpoints[1][0] | minus: 1-}}px)" {{-dataPrefixClass-}}srcset="{%- include 'includes/image/image_url', imgWidth: backupImgWidth, imgHeight: backupImgHeight, imgColumnWidth: backupImgColumnWidth, format: 'png' -%}">
	{%- else -%}
		<source type="image/jpeg" media="(max-width: {{-imageBreakpoints[1][0] | minus: 1-}}px)" {{-dataPrefixClass-}}srcset="{%- include 'includes/image/image_url', imgWidth: backupImgWidth, imgHeight: backupImgHeight, imgColumnWidth: backupImgColumnWidth, format: 'jpeg' -%}">
	{%- endif -%}
	<img {{dataPrefixClass}} src="
	{%- if imagePng -%}
		{%- include 'includes/image/image_url', format: 'png', imgWidth: backupImgWidth, imgHeight: backupImgHeight, imgColumnWidth: backupImgColumnWidth -%}
	{%- else -%}
		{%- include 'includes/image/image_url', format: 'jpeg', imgWidth: backupImgWidth, imgHeight: backupImgHeight, imgColumnWidth: backupImgColumnWidth -%}
	{%- endif -%}
	" alt="{{newAlt}}">
</picture>
{%- endif -%}
{% endcapture %}

{% comment %}Make the image a link if the link is defined{% endcomment %}
{%- if imageLink contains '/'  -%}
	<a href="{{imageLink}}">{{imageCapture}}</a>
{%- else -%}
	{{imageCapture}}
{%- endif -%}
