{%- capture productData -%}
{
    "unlimited": {{product.unlimited}},
    "quantity": {{product.quantity}},
    "sku": "{{product.sku}}"
}
{%- endcapture -%}
<div class="pc__product pc__product__single" id="pc__product{{product.ecwidid}}" itemscope itemtype="https://schema.org/Product" data-ecwid="{{product.ecwidid}}" {% if product.combinations.size>= 1 %}data-combinations="{{product.combinations | to_json | url_encode }}"{% endif %} data-product="{{productData | strip_newlines | url_encode }}">
	{%- include "platecommerce/product/modules/get_variation" -%}
	{%- include "platecommerce/modules/breadcrumb" -%}
	<div class="pc__product__wrapper">
		<div class="pc__product__media__wrapper">
			{%- include "platecommerce/product/modules/media_slider", imageSizes: "$-12-56%|md-7-64%|lg-7-64%|xl-7-64%|xxl-7-64%|xxxl-7-64%", lazyLoading: true -%}
			<div class="pc__product__info">
				{%- include "platecommerce/product/modules/title", titleTag: 'h2' -%}
				{%- include "platecommerce/product/modules/description" -%}
				{%- include "platecommerce/product/modules/attributes" -%}
			</div>
			<div class="author-container">
                <div class="author-inner fl-container fl-row jc-between">
                    <div class="share-socials typewriter fl-container fl-row ai-center">
						<p class="share-on">Deel dit product</p>
						<div class="nav-icon social-icon">
							<div class="nav-icon-bg"></div>
							<a class="copy-link" href="{{request.url | remove: 'https://' | remove: '://' }}" title="Kopieer URL">{% include "includes/icons/link_icon" %}</a>
						</div>
						<div class="nav-icon social-icon desktop">
							<div class="nav-icon-bg"></div>
							<a class="whatsapp" target="_blank" href="https://web.whatsapp.com/send?text={{request.url | remove: 'https://' | remove: '://' }}" title="Deel op whatsapp">{% include "includes/icons/whatsapp_icon" %}</a>
						</div>
						<div class="nav-icon social-icon mobile">
							<div class="nav-icon-bg"></div>
							<a class="whatsapp" target="_blank" href="whatsapp://send?text={{request.url | remove: 'https://' | remove: '://' }}" title="Deel op whatsapp">{% include "includes/icons/whatsapp_icon" %}</a>
						</div>
						<div class="nav-icon social-icon">
							<div class="nav-icon-bg"></div>
							<a class="facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{request.url | remove: 'https://' | remove: '://' }}" title="Deel op Facebook">{% include "includes/icons/facebook_icon" %}</a>
						</div>
                    </div>
                </div>
            </div>
		</div>
		<div class="pc__product__content">
			{%- include "platecommerce/product/modules/title", titleTag: 'h1' -%}
			{%- include "platecommerce/product/modules/categories" -%}
			{% comment %} {%- include "platecommerce/product/modules/sku" -%} {% endcomment %}
			{% if post.quantity <= 5 %}
				<div class="pc__product__labels">
					<div class="category-square">
						<p>{%- if post.quantity == 0 -%}Uitverkocht{%- else -%}Bijna uitverkocht{%- endif -%}</p>
					</div>
				</div>
			{% endif %}
			{% unless post.quantity == 0 %}
				<div class="pc__product__prices">
					{%- include "platecommerce/product/modules/price" -%}
				</div>
				{%- include "platecommerce/product/modules/variations" -%}
				{% comment %} {%- include "platecommerce/product/modules/quantity" -%} {% endcomment %}
				<div class="quantity-add-to-cart ">
					{%- include "platecommerce/product/modules/quantity-input" -%}
					{%- include "platecommerce/product/modules/add-to-cart" -%}
				</div>
			
				<div class="pc__product__usps">
					<ul class="pc__product__usps__list typewriter">
						{% for usp in site.shop_usps %}
							<li><p>{% include "includes/icons/check_icon" %}{{usp}}</p></li>
						{% endfor %}
					</ul>
				</div>
			{% endunless %}
		</div>
	</div>
</div>
{% if post.relatedproducts != blank %}
	<div class="pc__product__related plate--row">
		<div class="plate--column md-12">
			<div class="shop-title-container typewriter">
				<p>Gerelateerde producten</p>
			</div>
		</div>
		{% for product in post.relatedproducts, limit: 4 %}
			<div class="plate--column xs-12 sm-6 md-3">
				{% include "platecommerce/product/product_index", givenImageSizes: "$-12-80%|sm-6-80%|md-6-80%|lg-6-80%|xl-6-80%|xxl-6-80%|xxxl-6-80%", imageMode: "crop" %}
			</div>
		{% endfor %}
	</div>
{% endif %}
