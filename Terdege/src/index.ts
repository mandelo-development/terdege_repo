import '../../config/node_modules/regenerator-runtime/runtime';
import './scripts/scrollContainer';
import {platecommerce} from './scripts/platecommerce/platecommerce';
import { general, adScript, fileDrop } from './scripts/general';
async function faqs() {
	if (document.querySelector('.faq')) {
		let loadFaq = await import('./scripts/faq');
		loadFaq.faq();
	}
} 
import { swiperInstances } from './scripts/swiper';
import { filter } from './scripts/filter';
// import { form } from './scripts/form';
import { form } from './scripts/forms/apiForm';
import './scripts/functions';
import { instaFeed } from './scripts/instaFeed';
import { lazyload } from './scripts/lazyload';
import { spawnDots } from './scripts/spawnDots';
import { login } from './scripts/account';
import { shopEvents } from './scripts/shopEvents';
import { herkomstCode } from './scripts/herkomstCode';
import { gallery } from './scripts/gallery';
async function loadMessage() {
	if (document.querySelector('.popup_message')) {
		let loadMessagee = await import('./scripts/messages');
		loadMessagee.closeMessages();
	}
}
import { accountLogin, setDatalayer } from './scripts/accountLogin';
import './scripts/updatecss';
import './styles/style';
import { scrollCont } from './scripts/scrollContainer';


if (document.body.getAttribute('namespace') == 'coupon-index') {

} else {
	platecommerce();
	general();
	adScript();
	fileDrop();
	faqs();
	swiperInstances();
	filter();
	form();
	instaFeed();
	lazyload(document);
	spawnDots();
	login();
	shopEvents();
	herkomstCode();
	gallery();
	loadMessage();
	setDatalayer();
	// accountLogin();
	scrollCont.stop();
	setTimeout(() => {
		scrollCont.update();
		scrollCont.start();
	}, 250);
	
}