import {
	PlateCommerce
} from './platecommerce/scripts/functions/PlateCommerce'
let {
	Cart,
	OrderHandler,
	Config,
	Order
} = PlateCommerce
import {
	devLog
} from './platecommerce/scripts/functions/devlog'
import axios from '../../../config/node_modules/axios';
import {
	scrollCont
} from './scrollContainer';
import {
	getCookie,
	setCookie,
	checkCookie
} from './functions'
import { throttle } from './functions';

async function login() {
	var $ = require("../../../config/node_modules/jquery");

	// $(function() {
	// 	$(".emg__account-login__button").on("click", handleLogin)
	// 	console.log(localStorage);
	// })
	document.addEventListener("DOMContentLoaded", function () {
		var loginButton = document.querySelector(".emg__account-login__button");
		if (loginButton) {
			loginButton.addEventListener("click", (event) => {
				handleLogin(event)
			});
		}

		

		function handleLogin(event) {
			event.preventDefault();
			var formParent = event.target.closest(".emg__account");
			var email = formParent.querySelector(".emg__account-info__username").value;
			var password = formParent.querySelector(".emg__account-info__password").value;	
			d(email, password, formParent);
		}

		if (localStorage.getItem('currentLoggedIn')) {
			var currentUserData = JSON.parse(localStorage.getItem('currentLoggedIn'));
			let currentUserNav = document.querySelector(".read-more.current-user");
			let subscriptionButton = document.querySelector(".navbar-icons-inner__btns .subscription-button");
			let loginButton = document.querySelector(".navbar-icons-inner__btns .read-more.login");

			if(currentUserData !== null) {
				$("body").addClass("is-loggedin");
				let currentUserNavText = currentUserNav.querySelector(".current-username");

				// console.log({currentUserNavText})
				
				currentUserNavText.innerText = currentUserData.name;
				// currentUserNav.style.display = "block";
				$(currentUserNav).fadeIn(0);

				$('.current-user.dropdown').mouseenter(throttle(function () { 
					$('[aria-labelledby="account-dropdown"]').stop().fadeIn(500);
				}, 500));
				$('.current-user.dropdown').mouseleave(throttle(function () { 
					$('[aria-labelledby="account-dropdown"]').stop().fadeOut(500);
				}, 500));
				
				fillForm(currentUserData);


				let mobileLogout = document.querySelector(".mobile-log-out");
				var mediaQuery = window.matchMedia("(max-width: 767px)")
				if (mediaQuery.matches) {
					// mobileLogout.style.display = "block";
					$(mobileLogout).fadeIn(0);
				} else {
					// mobileLogout.style.display = "none";
					$(mobileLogout).fadeOut(0);
				}
			} else {
				// console.log("current-user === null")
				// console.log({subscriptionButton}, {loginButton})
				// $(subscriptionButton).fadeIn(400);
				// $(loginButton).fadeIn(400);
				$(subscriptionButton).animate({opacity: '1'}, 0);
				$(loginButton).animate({opacity: '1'}, 0);
			}

			setTimeout(() => {
				if (document.querySelector('.pc__account-login__wrapper')) {
					console.log(document.querySelector('.pc__account-login__wrapper'))
				   document.querySelector('.pc__account-login__wrapper').style.display = 'none';
				}
			}, 500)
			// console.log(currentLoggedInUser);
			$('.current-user-log-out').click(function () {
				localStorage.removeItem("currentLoggedIn");
				$(".read-more.current-user").fadeOut();
				window.location.reload();
				$("body").removeClass("is-loggedin");
			});
		} else {
			
		
			$('.current-user-log-out').click(function () {
				localStorage.removeItem("currentLoggedIn");
				$(".read-more.current-user").fadeOut();
				window.location.reload();
				$("body").removeClass("is-loggedin");
			});
		}
	});

	$(".pc__account-login__wrapper input[type='radio']").each(function(index, field) {
		$(field).change(function() {
			if ($(field).is(":checked") && $(field).val() === 'on') {
				$('.pc__account-login__screen').show();
			} else {
				$('.pc__account-login__screen').hide();
			}
			scrollCont.update();
		});
	});


	// function handleLogin(event) {
	// 	event.preventDefault();
	// 	var formParent = $(event.target).parents(".emg__account")
	// 	var email = formParent.find(".emg__account-info__username").val()
	// 	var password = formParent.find(".emg__account-info__password").val()
	// 	// retrieveData(email, password, formParent);

	// 	console.log({formParent, email, password})
		
	// 	d(email, password, formParent);
	// }

	function retrieveData(email, password, form) {
		// var ajaxUrl;
		// if (window.location.href.indexOf("-dev.mijnmandelosite.nl") > -1) {
		// 	//does contain
		// 	ajaxUrl = "https://7e5m96yrk4.execute-api.eu-west-1.amazonaws.com/Prod/ksapi_validate_td_abonnee";
		// } else {
		// 	ajaxUrl = "https://f2satec32i.execute-api.eu-west-1.amazonaws.com/Prod/ksapi_validate_td_abonnee";
		// }

		// var result = $.ajax({
		// 	url: ajaxUrl,
		// 	type: "POST",
		// 	contentType: "application/json",
		// 	data: JSON.stringify({
		// 		email: email,
		// 		password: password
		// 	}),
		// 	success: function(result) {
		// 		console.log(result);
		// 		fillForm(result);
		// 		formSucces(form, result)
		// 	},
		// 	error: function(result) {
		// 		console.log(result);
		// 		formError(form);
		// 	}
		// });

		// var endpointUrl;
		var endpointUrls;
		if (window.location.href.indexOf("-dev.mijnmandelosite.nl") > -1) {
			//does contain
			// endpointUrl = 'https://t-ksapi.erdee.nl/v2/accounts/login';
			// endpointUrls = ['https://t-ksapi.erdee.nl/v2/accounts/login', ];

			endpointUrls = ['https://t-ksapi.erdee.nl/v2/accounts/login', `https://t-ksapi.erdee.nl/v2/accounts/${id}/authorizations/${serviceId}`];
		} else {
			// endpointUrl = 'https://ksapi.erdee.nl/v2/accounts/login';
			endpointUrls = [`https://ksapi.erdee.nl/v2/accounts/login`, `https://ksapi.erdee.nl/v2/accounts/${id}/authorizations/${serviceId}`];
		}

		// d()

		/// PREVIOUS CALL

		// axios({
		// 	method: 'post',
		// 	url: endpointUrl,
		// 	// cors: true,
		// 	data: JSON.stringify({
		// 		email: email,
		// 		password: password
		// 	}),
		// 	headers: {
		// 		'X-Origin-AuthToken': 'PUitIB5ogScu7c0Rnot0ZCrwLgqVNeBB',
		// 		'Content-Type': 'application/json',
		// 	},
		// }).then(response => {
		// 	// clearForm(form)
		// 	$(loader).hide();

		// 	console.log({response});

		// 	var successMessage = response.data.user_message;
		// 	// displayMessage(successMessage, true, true);

		// 	fillForm(result);
		// 	formSucces(form, response.data.accounts[0])

		// 	// formEmgSucces(formParent, response.data.accounts[0]);

		// 	form.submit();

		// }).catch(error => {

		// 	// Check specific HTTP error status codes and log them
		// 	// var httpStatus = error.response && error.response.status;
		// 	// switch (httpStatus) {
		// 	// 	case 401:
		// 	// 	case 403:
		// 	// 	case 405:
		// 	// 	case 406:
		// 	// 	case 415:
		// 	// 	case 422:
		// 	// 		if (error.response.data.debug !== undefined) {
		// 	// 			console.log("HTTP Error Status Code:", httpStatus, ":", error.response.data.error_code, "," + error.response.data.debug);
		// 	// 		} else {
		// 	// 			console.log("HTTP Error Status Code:", httpStatus, ":", error.response.data.error_code);
		// 	// 		}
		// 	// 		break;
		// 	// 	default:
		// 	// 		// Handle other HTTP status codes if needed
		// 	// 		break;
		// 	// }

		// 	$(loader).hide();

		// 	console.log({error});
		// 	// console.log(error.response.data.user_message);
		// 	if (error.response && error.response.data && error.response.data.user_message) {
		// 		// Display the error message to the user
		// 		var errorMessage = error.response.data.user_message;
		// 		displayMessage(errorMessage, false);
		// 	} else {
		// 		// Handle other errors
		// 		// console.log("An error occurred:", error.message);
		// 		var errorMessage = "Er is iets misgegaan, neem contact op met de klantenservice.";
		// 		displayMessage(errorMessage, false);
		// 	}
		// })

		/// END PREVIOUS CALL

	}

	async function d(email, password, form) {
		try {
			var loader = $(form).find('.form-loader');
			$(loader).show();
		
			var host, xOriginToken;
			if (window.location.href.indexOf("-dev.mijnmandelosite.nl") > -1) {
				host = "https://t-ksapi.erdee.nl";
				xOriginToken = "PUitIB5ogScu7c0Rnot0ZCrwLgqVNeBB";
			} else {
				host = "https://ksapi.erdee.nl";
				xOriginToken = "PlCLXmGC6WgR5ZEbC1gbIXhKaIH6Ww5T";
			}
			
			// console.log({host})

			const loginResponse = await axios({
				method: 'post',
				url: `${host}/v2/accounts/login/person`,
				// url: `${host}/v2/accounts/login/{orderTypeFirst}`,
				data: JSON.stringify({
					email: email,
					password: password,
				}),
				headers: {
					'Content-Type': 'application/json',
					'X-Origin-AuthToken': xOriginToken,
				},
			});

			// console.log({loginResponse})
		
			const ksId = loginResponse.data.accounts[0].id;
			const serviceId = "terdege.nl";
			const token = loginResponse.data.accounts[0].token;
			
			const getUserResponse = await axios({
				method: 'get',
				url: `${host}/v2/accounts/${ksId}/authorizations/${serviceId}`,
				headers: {
					'Content-Type': 'application/json',
					'X-Origin-AuthToken': xOriginToken,
					'X-KSAPI-Authorization': `client-token ${token}`,
				},
			});


			$(loader).hide();
			displayMessage(form, "", true, true);
		
			// console.log({getUserResponse})

			formSucces(form, getUserResponse.data, loginResponse.data);
			
		
		} catch (error) {
			console.error('Error:', error);
	  
			// Check specific HTTP error status codes and log them
			var httpStatus = error.response && error.response.status;
			switch (httpStatus) {
				case 401:
				case 403:
				case 405:
				case 406:
				case 415:
				case 422:
				if (error.response.data.debug !== undefined) {
					console.error("HTTP Error Status Code:", httpStatus, ":", error.response.data.error_code, ", " + error.response.data.debug);
				} else {
					console.error("HTTP Error Status Code:", httpStatus, ":", error.response.data.error_code);
				}
				break;
				default:
				// Handle other HTTP status codes if needed
				break;
			}
			
			if (error.response && error.response.data && error.response.data.user_message) {
				var errorMessage = error.response.data.user_message;
				displayMessage(form, errorMessage, false);
			} else {
				var errorMessage = "Er is iets misgegaan, neem contact op met de klantenservice.";
				displayMessage(form, errorMessage, false);
			}

			$(loader).hide();
		}
	}

	// isAbonnee: true/false
	// potentialDiscountPercentage: 1.0,
	// couponCode: LEDENVOORDEELBLABLABL
	

	

	function formError(f) {
		var screen = f;
		screen.addClass('error');
		// console.log(screen.find('.error-message'))
		screen.find('.error-message').show();
		Cart.deleteDiscountCouponCode(localStorage.getItem('member_coupon'));
		const totalChanged = new Event('totalChanged');
		document.dispatchEvent(totalChanged);
		setEvent({}, "invalidAbonnee")
	}

	function formSucces(f, resultUser, resultLogin) {
		// console.log(f, resultUser, resultLogin)
		var user, jsonUser, tdAuth;
		
		user = resultLogin.accounts[0];
		user['auth'] = resultUser;
		jsonUser = JSON.stringify(user);
		// console.log(jsonUser)
		localStorage.setItem('currentLoggedIn', jsonUser);
		if (f.tagName.toLowerCase() === 'form') {
			f.submit();
		}

		if (f.closest('.checkout__wrapper') !== null) {
			fillForm(user);
		}
		
		// addListeners();
		// console.log(user)
		
		// if (resultUser.authorizations['terdege.nl']) {
		// 	tdAuth = resultUser.authorizations['terdege.nl'].full;
		// } else {
		// 	tdAuth = resultLogin.accounts[0].auth
		// }
		// console.log(tdAuth);
		
		// if (tdAuth.includes("subscriber") === true) {
			// var coupon = atob(document.querySelector(".pc__checkout").getAttribute("data-coupon"));
			// Cart.addDiscountCouponCode(coupon);
			// localStorage.setItem('member_coupon', coupon);
			// setEvent(result, "isAbonnee")
		// } else if (tdAuth.includes("subscriber") === false) {
			// Cart.deleteDiscountCouponCode(localStorage.getItem('member_coupon'));
			// setEvent(result, "isNotAbonnee")
			// localStorage.setItem('member_coupon', false);
		// } else {
			// Cart.deleteDiscountCouponCode(localStorage.getItem('member_coupon'));
			// setEvent(result, "invalidAbonnee")
			// localStorage.setItem('member_coupon', false);
		// }
		const totalChanged = new Event('totalChanged');
		document.dispatchEvent(totalChanged);
	}

	function fillForm(data) {
		// console.log(data);

		var fieldMapping = {
			email: "email",
			firstname: "voornaam",
			lastname: "achternaam",
			address: {
				street: "straatnaam",
				street_number: "huisnummer",
				street_number_extra: "toevoeging",
				town: "woonplaats",
				zipcode: "postcode",
			},
		}

		// hier nog factuur adres invoeren
		// console.log(fieldMapping)
		
		for (const [zenoKey, formKey] of Object.entries(fieldMapping)) {
			// console.log(zenoKey, formKey, data)
			if (zenoKey in data) {
				// console.log(zenoKey)
				if (zenoKey == 'address') {
					for (const [zKey, iKey] of Object.entries(formKey)) {
						// console.log(zKey, iKey);
						var field = $("[zenokey='" + iKey + "']");
						field.val(data['address'][zKey])
						if ($(field).val()) {
							field.parents('.form-field').addClass('form-field--is-filled')
						}
					}
				} else {
					var field = $("[zenokey='" + formKey + "']");
					field.val(data[zenoKey])
					if ($(field).val()) {
						field.parents('.form-field').addClass('form-field--is-filled')
					}
				}
			}
		}
	}

	document.addEventListener('orderSuccesfull', function() {
		localStorage.setItem('member_coupon', false);
	})

	function displayMessage(form, message, success, hide) {
		var formMessage = $(form).find('.form-notification');
		var formMessageText = formMessage.find('.form-notification__text')

		formMessageText.text(message);

		if(hide !== true) {
			if (success === true) {
				$(formMessage).removeClass("form-notification--failed");
				$(formMessage).addClass("form-notification--success");
				$(formMessage).show();
			} else {
				$(formMessage).removeClass("form-notification--success");
				$(formMessage).addClass("form-notification--failed");
				$(formMessage).show();
			}
		} else {
			(formMessage).hide();
		}

	}

}

function setEvent(data, name) {
	let event = new CustomEvent(name, {
		"detail": data
	});
	document.dispatchEvent(event);
}

function addListeners() {
	let couponEventListeners = [{
		"name": "isAbonnee",
		"event": "isAbonnee"
	}, {
		"name": "isNotAbonnee",
		"event": "isNotAbonnee"
	}, {
		"name": "invalidAbonnee",
		"event": "invalidAbonnee"
	}]

	console.log(couponEventListeners);

	couponEventListeners.forEach((listener, i) => {
		document.addEventListener(listener.event, function(e) {
			devLog((listener.name), {
				details: e.detail,
				eventName: listener.event
			})
		})
	});
}



export {
	login
}