import {
	devLog
} from './platecommerce/scripts/functions/devlog'
import {
	scrollCont
} from './scrollContainer';
import axios from '../../../config/node_modules/axios';
import { throttle } from './functions';

async function accountLogin() {
	var $ = require("../../../config/node_modules/jquery");

	$(function() {
		$("form.emg__account").on("submit", handleEmgLogin)
	})

	function handleEmgLogin(event) {
		event.preventDefault();
		
		var formParent = $(event.target).find(".emg__account-login__screen")
		var email = formParent.find(".emg__account-info__username").val()
		var password = formParent.find(".emg__account-info__password").val()

		// console.log(event, formParent, email, password);
		// retrieveEmgData(email, password, formParent, event.target);
		
	}

	// function retrieveEmgData(email, password, formParent, form) {
	// 	console.log({email: email}, {password: password})
	// 	var ajaxUrl;
	// 	if (window.location.href.indexOf("-dev.mijnmandelosite.nl") > -1) {
	// 		//does contain
	// 		ajaxUrl = "https://7e5m96yrk4.execute-api.eu-west-1.amazonaws.com/Prod/ksapi_validate_td_abonnee";
	// 		// ajaxUrl = "https://ksapi.erdee.nl/v2/accounts/login";
	// 		// ajaxUrl = "https://w6d6ydqh2d.execute-api.eu-west-1.amazonaws.com/Prod/ksapi-validate-td";
	// 		// ajaxUrl = "https://f2satec32i.execute-api.eu-west-1.amazonaws.com/Prod/ksapi_validate_td_abonnee"
	// 	} else {
	// 		ajaxUrl = "https://f2satec32i.execute-api.eu-west-1.amazonaws.com/Prod/ksapi_validate_td_abonnee";
	// 	}

	// 	var result = $.ajax({
	// 		url: ajaxUrl,
	// 		type: "POST",
	// 		contentType: "application/json",
	// 		data: JSON.stringify({
	// 			email: email,
	// 			password: password
	// 		}),
	// 		success: function(result) {
	// 			console.log(result);
	// 			// fillEmgForm(result);
	// 			formEmgSucces(formParent, result);
	// 			form.submit();
	// 		},
	// 		error: function(result) {
	// 			// console.log(result);
	// 			console.log(result)
	// 			formEmgError(formParent);
	// 		}
	// 	});

	// 	console.log(result);
	// }


	function retrieveEmgData(email, password, formParent, form) {
		var loader = $(form).find('.form-loader');
		var endpointUrl;
		if (window.location.href.indexOf("-dev.mijnmandelosite.nl") > -1) {
			//does contain
			endpointUrl = 'https://t-ksapi.erdee.nl/v2/accounts/login';
		} else {
			endpointUrl = 'https://ksapi.erdee.nl/v2/accounts/login';
		}
		
		// var result = $.ajax({
		// 	url: "https://ksapi.erdee.nl/v2/accounts/login",
		// 	type: "POST",
		// 	headers: {
		// 		'Content-Type':'application/x-www-form-urlencoded',
		// 		'X-Origin-AuthToken': 'PUitIB5ogScu7c0Rnot0ZCrwLgqVNeBB',
		// 	},
		// 	data: JSON.stringify({
		// 		email: email,
		// 		password: password
		// 	}),
		// 	success: function(result) {
		// 		console.log(result);
		// 		// fillEmgForm(result);
		// 		formEmgSucces(formParent, result);
		// 		form.submit();
		// 	},
		// 	error: function(result, status) {
		// 		console.log(result, status);
		// 		formEmgError(formParent);
		// 	}
		// });

		// axios({
		// 	method: 'post',
		// 	url: endpointUrl,
		// 	// cors: true,
		// 	data: JSON.stringify({
		// 		email: email,
		// 		password: password
		// 	}),
		// 	headers: {
		// 		'X-Origin-AuthToken': 'PUitIB5ogScu7c0Rnot0ZCrwLgqVNeBB',
		// 		'Content-Type': 'application/json',
		// 	},
		// }).then(response => {
		// 	// clearForm(form)
		// 	$(loader).hide();

		// 	console.log({response});
			
		// 	var successMessage = response.data.user_message;
		// 	displayMessage(successMessage, true, true);

		// 	formEmgSucces(formParent, response.data.accounts[0]);

		// 	form.submit();

		// }).catch(error => {

		// 	// Check specific HTTP error status codes and log them
		// 	// var httpStatus = error.response && error.response.status;
		// 	// switch (httpStatus) {
		// 	// 	case 401:
		// 	// 	case 403:
		// 	// 	case 405:
		// 	// 	case 406:
		// 	// 	case 415:
		// 	// 	case 422:
		// 	// 		if (error.response.data.debug !== undefined) {
		// 	// 			console.log("HTTP Error Status Code:", httpStatus, ":", error.response.data.error_code, "," + error.response.data.debug);
		// 	// 		} else {
		// 	// 			console.log("HTTP Error Status Code:", httpStatus, ":", error.response.data.error_code);
		// 	// 		}
		// 	// 		break;
		// 	// 	default:
		// 	// 		// Handle other HTTP status codes if needed
		// 	// 		break;
		// 	// }

		// 	$(loader).hide();

		// 	console.log({error});
		// 	// console.log(error.response.data.user_message);
		// 	if (error.response && error.response.data && error.response.data.user_message) {
		// 		// Display the error message to the user
		// 		var errorMessage = error.response.data.user_message;
		// 		displayMessage(errorMessage, false);
		// 	} else {
		// 		// Handle other errors
		// 		// console.log("An error occurred:", error.message);
		// 		var errorMessage = "Er is iets misgegaan, neem contact op met de klantenservice.";
		// 		displayMessage(errorMessage, false);
		// 	}
		// })

		function displayMessage(message, success, hide) {
			var formMessage = $(form).find('.form-notification');
			var formMessageText = formMessage.find('.form-notification__text')
	
			formMessageText.text(message);
	
			if(hide !== true) {
				if (success === true) {
					$(formMessage).removeClass("form-notification--failed");
					$(formMessage).addClass("form-notification--success");
					$(formMessage).show();
				} else {
					$(formMessage).removeClass("form-notification--success");
					$(formMessage).addClass("form-notification--failed");
					$(formMessage).show();
				}
			} else {
				(formMessage).hide();
			}

		}
	}



	function formEmgError(f) {
		var screen = f;
		screen.addClass('error');
		screen.find('.error-message').show();
		setEmgEvent({}, "isNotLoggedIn");
		scrollCont.update();
	}

	function formEmgSucces(f, result) {
		var screen = f;
		// screen.removeClass('error');
		// screen.find('.error-message').hide();
		
		// addEmgListeners()
		// setEmgEvent(result, "isLoggedIn");
		
		var userData;
		let displayName;
		let email;

		if(result.name !== '') {
			displayName = result.name;
		} else if(result.email !== '' ) {
			displayName = result.email;
		} else if(result.lastname !== '') {
			displayName = result.lastName;
		} else if(result.initials !== '') {
			displayName = result.initials;
		} else {
			displayName = "Gebruiker";
		}
		
		email = result.email,

		userData = {
			name: result.name,
			initials: result.initials,
			middleName: result.middleName,
			lastName: result.lastName,
			email: result.email,
			zeno_id: result.zeno_client_id,
			ks_id: result.ks_id,
			role: result.role,
			token: result.token,
		};

		var userDataJSON = JSON.stringify(userData);
		sessionStorage.setItem("currentLoggedIn", userDataJSON);

		
		// console.log(sessionStorage.getItem("currentLoggedIn"));
		

		// if (result.isAbonnee == true) {
		// 	// Cart.addDiscountCouponCode(result.couponCode);
		// 	// localStorsessionStorageage.setItem('member_coupon', result.couponCode);
		// 	setEmgEvent(result, "isAbonnee")
		// } else if (result.isAbonnee == false) {
		// 	// Cart.deleteDiscountCouponCode(sessionStorage.getItem('member_coupon'));
		// 	setEmgEvent(result, "isNotAbonnee")
		// 	// sessionStorage.setItem('member_coupon', false);
		// } else {
		// 	// Cart.deleteDiscountCouponCode(sessionStorage.getItem('member_coupon'));
		// 	setEmgEvent(result, "invalidAbonnee")
		// 	// sessionStorage.setItem('member_coupon', false);
		// }
	}

	$('.current-user.dropdown').mouseenter(throttle(function () { 
		$('[aria-labelledby="account-dropdown"]').stop().fadeIn(500);
	}, 500));
	$('.current-user.dropdown').mouseleave(throttle(function () { 
		$('[aria-labelledby="account-dropdown"]').stop().fadeOut(500);
	}, 500));

	$('.current-user-log-out').click(function () {
		sessionStorage.removeItem("currentLoggedIn");
		// $(".read-more.current-user").fadeOut();
		window.location.reload();
		// $("body").removeClass("is-loggedin");
    });
}

// function setEmgEvent(data, name) {
// 	// console.log({data}, {name})
// 	let event = new CustomEvent(name, {
// 		"detail": data
// 	});
// 	document.dispatchEvent(event);
// }

// function addEmgListeners() {
// 	let loginEventListeners = [{
// 	// 	"name": "isAbonnee",
// 	// 	"event": "isAbonnee"
// 	// }, {
// 	// 	"name": "isNotAbonnee",
// 	// 	"event": "isNotAbonnee"
// 	// }, {
// 	// 	"name": "invalidAbonnee",
// 	// 	"event": "invalidAbonnee"
// 	// }, {
// 		"name": "isLoggedIn",
// 		"event": "isLoggedIn"
// 	}, {
// 		"name": "isNotLoggedIn",
// 		"event": "isNotLoggedIn"
// 	}]
// 	loginEventListeners.forEach((listener, i) => {
// 		document.addEventListener(listener.event, function(e) { 
// 			devLog((listener.name), {
// 				details: e.detail,
// 				eventName: listener.event
// 			})
// 		})
// 	});
// }

// function isLoggedIn(user) {
// 	alert('test')
// }
// document.addEventListener("DOMContentLoaded", function() {
// 	let currentUser = sessionStorage.getItem("currentLoggedIn");
// 	let currentUserData = JSON.parse(currentUser);
// 	let currentUserNav = document.querySelector(".read-more.current-user");
// 	let subscriptionButton = document.querySelector(".navbar-icons-inner__btns .subscription-button");
// 	let loginButton = document.querySelector(".navbar-icons-inner__btns .read-more.login");
// 	console.log({currentUserData})

	
	
// 	if(currentUserData !== null) {
// 		$("body").addClass("is-loggedin");
// 		let currentUserNavText = currentUserNav.querySelector(".current-username");

// 		// console.log({currentUserNavText})
		
// 		currentUserNavText.innerText = currentUserData.name;
// 		// currentUserNav.style.display = "block";
// 		$(currentUserNav).fadeIn(0);
		
// 		fillFormData(currentUserData);

// 		let mobileLogout = document.querySelector(".mobile-log-out");
// 		var mediaQuery = window.matchMedia("(max-width: 767px)")
// 		if (mediaQuery.matches) {
// 			// mobileLogout.style.display = "block";
// 			$(mobileLogout).fadeIn(0);
// 		} else {
// 			// mobileLogout.style.display = "none";
// 			$(mobileLogout).fadeOut(0);
// 		}
// 	} else {
// 		// console.log("current-user === null")
// 		// console.log({subscriptionButton}, {loginButton})
// 		// $(subscriptionButton).fadeIn(400);
// 		// $(loginButton).fadeIn(400);
// 		$(subscriptionButton).animate({opacity: '1'}, 0);
// 		$(loginButton).animate({opacity: '1'}, 0);
// 	}
// });



function setDatalayer() {
	// "event": {
	// 	"isLoggedIn": "<true/false>",
	// 	"subscriptionType":"<Name/id of the type of subscription>",
	// },
	// "user": {
	// 	"customerKSId": "<customerKSId>",
	// 	"customerZenoId": "<customerZenoId>",
	// 	"hashedEmail": "<hashedEmail>"
	// }

	window.dataLayer = window.dataLayer || [];
	// inlog data
	pageview();
	
	// post data
	if (document.getElementsByTagName('body')[0].getAttribute('namespace').includes('article-show')) {
		var postDetailViewId = document.getElementsByTagName('body')[0].getAttribute('data-post-id'),
		postDetailViewNamespace = 'geen categorie',
		postDetailViewTitle;

		if (document.getElementsByTagName('body')[0].getAttribute('data-post-cat') !== '') {
			postDetailViewNamespace = document.getElementsByTagName('body')[0].getAttribute('data-post-cat');
		}

		if (document.querySelectorAll('h1').length) {
			postDetailViewTitle = document.querySelector('h1').innerHTML;
		} else {
			var postDetailViewTitles = window.location.href.split('/');
			postDetailViewTitle = postDetailViewTitles[postDetailViewTitles.length - 1];
			if (!postDetailViewTitle) {
				postDetailViewTitle = 'Terdege Homepage';
			}
			// console.log(postDetailViewNamespace);
		}
		pushDetailView(postDetailViewId, postDetailViewTitle, postDetailViewNamespace);
	}
	
	// newsletter data
	for (const newsLetter of document.querySelectorAll(".news-form-container")) {
		var newsletterForm = newsLetter.querySelector('form'),
			newsletterFormInputs = newsLetter.querySelectorAll('input:not(.button)'),
			counterNewsLetter = 0;
		
		for (const newsletterFormInput of newsletterFormInputs) {
			newsletterFormInput.addEventListener('input', () => {
				counterNewsLetter++
				if (counterNewsLetter == 1) {
					pushDetailViewNewsLetter('Enter details');
					// pushDetailViewNewsLetter('Confirmation');
				}
			})
		}

		newsletterForm.addEventListener('submit', function(event) {
			event.preventDefault();
			pushDetailViewNewsLetter('Thank you');
			// pushDetailViewNewsLetter('Confirmation');
		})
	}

	// link data
	var targetIdLink;
	for (const link of document.querySelectorAll("a")) {
		if (link.href.includes("/abonnementen")){
			link.addEventListener('click', (e) => {
				// e.preventDefault();
				targetIdLink = link.innerHTML;
				pushDetailClick(targetIdLink)
			})
		}
	}
}
function pageview() {
	dataLayer.push({
		"event": "pageview",
		"meta": {
			"event": {},
        	"user": {}
		}
	});
}
function pushDetailView(postDetailViewId, postDetailViewTitle, postDetailViewNamespace) {
	// console.log(postDetailViewId, postDetailViewTitle, postDetailViewNamespace);
	dataLayer.push({
		"event": "detailView",
		"meta": {
			"event": {},
        	"user": {}
		},
		"target": {
			"id": postDetailViewId,
			"type": 'article',
			"meta": {
				"title": postDetailViewTitle,
				"type": postDetailViewNamespace,						// column, blog, etc.
			}
		}
	});
	devLog(dataLayer);
}
function pushDetailClick(targetIdLink) {
	dataLayer.push({
		"event": "detailClick",
		"meta": {
			"event": {},
        	"user": {}
		},
		"target": {
			"id": targetIdLink,
			"type": "Button",
			"meta": {}
		}
	});
	devLog(dataLayer);
}


function pushDetailViewNewsLetter(id) {
	dataLayer.push({
		"event": "detailView",
		"meta": {
			"event": {},
        	"user": {}
		},
		"target": {
			"id": id,
			"type": "Newsletter signup - Funnel",
			"meta": {}
		}
	});
	devLog(dataLayer);
}
export {
	accountLogin, setDatalayer
}