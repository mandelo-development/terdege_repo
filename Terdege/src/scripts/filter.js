import {scrollCont} from './scrollContainer';
import {debounce} from './functions';
import { adScript } from './general';

var $ = require("../../../config/node_modules/jquery");

async function filter() {
	var selectedFilters = [];
	selectedFilters['page'] = 1;
	var taxonmyFilters = document.querySelectorAll(".filter-button");
	var taxonomyFiltersSelect = document.getElementById("taxonomy__select__input");
	var dateFilterFrom = document.getElementById("date__filter__from");
	var dateFilterTo = document.getElementById("date__filter__to");
	var postType;
	var theEnd = false;
	var appended = false;
	var clickedLoadMore = false;
	var container = document.getElementById("result__wrapper");
	var i = 0;


	if ($(container).length > 0) {
		function makeRequest(data, type) {

			var text = [];
			var i;
			for (i in data) {
				if (data.hasOwnProperty(i)) {
					text.push(i + "=" + encodeURIComponent(data[i]));
				}
			}
			var textState = [];
			var i;
			for (i in data) {
				if (data.hasOwnProperty(i)) {
					if (i != 'page') {
						textState.push(i + "=" + encodeURIComponent(data[i]));
					}
				}
			}

			console.log(textState);
			text = text.join("&");
			textState = textState.join("&");
			var url = "/filters?" + text;
			var pushStateUrl = "?" + textState;
			var loader = "<div class='loader'></div>";

			window.history.pushState(null, null, pushStateUrl);

			var xhr = window.XMLHttpRequest ? new XMLHttpRequest : window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : (alert("Browser does not support XMLHTTP."), false);
			xhr.onreadystatechange = text;
			$(container).append(loader);
			xhr.open("GET", url, true);
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.send(text);
			xhr.onload = function() {
				setTimeout(function() {
					container.classList.add("fade-out");
					if (type == 'append') {
						container.innerHTML = container.innerHTML + xhr.response;
						if (!xhr.response.includes('+==THEEND==+')) {
							setTimeout(function() {
								appended = false;
							}, 1000);
						} else {
							theEnd = true;
						}
					} else {
						container.innerHTML = xhr.response;
					}
					setTimeout(function() {
						$('.loader').remove();
						container.classList.remove('fade-out');
						scrollCont.update();
					}, 50);
				}, 100);
			};
			xhr.onerror = function() {
				alert("error");
			};
		}

		function loadNextPage(scrollY) {
			// console.log(appended, scrollY, theEnd);
			// console.log(theEnd);
			let resultElement = document.querySelector('#result__wrapper');
			let position = elementInViewport(resultElement, resultElement.offsetTop, resultElement.offsetHeight, 100, scrollY);
			// console.log(position)
			if (position && !appended && !theEnd) {
				appended = true;
				selectedFilters['page'] = selectedFilters['page'] + 1;
				makeRequest(selectedFilters, 'append');
			}
		}

		var loadMoreBtn = document.querySelector('.result__container .load-more a');
		if (loadMoreBtn) {
			loadMoreBtn.addEventListener('click', function (clickEvent) {
				clickEvent.preventDefault();
				this.classList.add('hide-btn');
				postType = document.querySelector('body').getAttribute('namespace').replace('-index', '');
				// console.log(postType)
				// console.log(selectedFilters.categories)
				selectedFilters.data_object = postType;
				if (selectedFilters.categories === undefined) {
					selectedFilters.categories = 'all';
				}
				loadNextPage(false);
				clickedLoadMore = true;
			});
		}

		if (document.querySelector('body').classList.contains('paginate_index')) {
			[$(document), scrollCont].forEach(container => {
				container.on('scroll', function (scrollEvent) {
					if (clickedLoadMore) {
						var scrollYOffset;
						if (container[0]) {
							scrollYOffset = window.pageYOffset;
						} else {
							scrollYOffset = scrollEvent.scroll.y;
						}
						loadNextPage(scrollYOffset);
					}
				})
			});
		}

		if (taxonmyFilters !== null) {
			for (; i < taxonmyFilters.length; i++) {
				postType = taxonmyFilters[i].getAttribute("data-object");
				selectedFilters.data_object = postType;
				taxonmyFilters[i].addEventListener("click", debounce(function (e) {
					selectedFilters['page'] = 1;
					appended = false;
					theEnd = false;

					$(taxonmyFilters).removeClass('active');
					$(this).addClass('active');

					var parentTax = this.getAttribute("data-parent");
					var buttonId = this.getAttribute("value");
					selectedFilters[parentTax] = buttonId;

					makeRequest(selectedFilters);
				}, 100));
			}
		}

		if (taxonomyFiltersSelect !== null) {
			taxonomyFiltersSelect.addEventListener('change', debounce(function(event) {
				selectedFilters['page'] = 1;
				appended = false;
				theEnd = false;
				var parentTax = this.getAttribute("data-parent");
				var sortValue = this.options[this.selectedIndex].value;

				postType = this.getAttribute("data-object");
				selectedFilters[parentTax] = sortValue;
				selectedFilters.data_object = postType;

				makeRequest(selectedFilters);
			}, 500));
		}

		if (dateFilterFrom !== null) {
			dateFilterFrom.addEventListener("change", debounce(function(dateFromEvent) {
				selectedFilters['page'] = 1;
				var parentTax = this.getAttribute("data-parent");
				var dateValue = this.value;
				appended = false;
				theEnd = false;

				postType = this.getAttribute("data-object");
				selectedFilters[parentTax] = dateValue;
				selectedFilters.data_object = postType;

				makeRequest(selectedFilters);
			}, 500));
		}

		if (dateFilterTo !== null) {
			dateFilterTo.addEventListener('change', debounce(function(dateToEvent) {
				selectedFilters['page'] = 1;
				var parentTax = this.getAttribute("data-parent");
				var dateValue = this.value;
				appended = false;
				theEnd = false;

				postType = this.getAttribute("data-object");
				selectedFilters[parentTax] = dateValue;
				selectedFilters.data_object = postType;

				makeRequest(selectedFilters);
			}, 500));
		}

		$(function() {
			var paramFilters = [];
			// Filter based on parameters in URL
			var param_object = getUrlParameter('data_object');
			var param_categories = getUrlParameter('categories');
			if (param_categories) {
				selectedFilters['categories'] = param_categories;
			}
			var param_date_from = getUrlParameter('date_input_from');
			var param_date_to = getUrlParameter('date_input_to');

			if (param_object || param_categories || param_date_from || param_date_to) {

				paramFilters.data_object = param_object;

				if (param_categories !== undefined) {
					var categoriesElement = $('*[value="' + param_categories + '"]');

					paramFilters.categories = param_categories;

					$(taxonmyFilters).removeClass('active');
					$(categoriesElement).addClass('active');

					if(categoriesElement[0].nodeName == "OPTION") {
						$(categoriesElement).prop("selected", "selected");
					}
				}

				if (param_date_from !== undefined) {
					$('#date__filter__from').val(param_date_from);

					paramFilters.date_input_from = param_date_from;
				}

				if (param_date_to !== undefined) {
					$('#date__filter__to').val(param_date_to);

					paramFilters.date_input_to = param_date_to;
				}

				makeRequest(paramFilters);
			}

		});

		// Get parameters from URL
		function getUrlParameter(sParam) {
			var sPageURL = window.location.search.substring(1),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;

			for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');

				if (sParameterName[0] === sParam) {
					return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
				} else {}
			}
		}
	}

	function elementInViewport(el, top, height, offset, scrollY) {
		if (document.querySelector('body').getAttribute('namespace').replace('-index', '') == 'event') {
			offset = 0;
		} 
		// console.log(offset);
		while (el.offsetParent) {
			el = el.offsetParent;
			top += el.offsetTop;
		}

		if (scrollY === false) {
			return true;
		} else {
			return (
				(scrollY + window.innerHeight) > (height + top - offset)
			);
		}
	}
} export {
	filter
}