import { scrollCont } from './scrollContainer';
import { debounce } from './functions';

var $ = require("../../../config/node_modules/jquery");

async function form() {

    $(document).on('change', 'form input#zipcode, form input#street_number', function () {
        var form = $(this).closest('form');
        updateAddress(form);
    })

    function updateAddress(form) {
        var zipRegex = /^(\d{4})\s*([A-Z]{2})$/i;
        var numberRegex = /^(0|[1-9]\d*)$/;
        var zipField = $(form).find('input#zipcode');
        var numField = $(form).find('input#street_number');
        var strField = $(form).find('input#street');
        var twnField = $(form).find('input#town');
        var zip = zipField.val().replace(/(^\s+|\s+$)/g, '').replace(/ /g, '');
        var num = numField.val().replace(/(^\s+|\s+$)/g, '');
        if (!zip || !num || !zip.match(zipRegex) || !num.match(numberRegex))
            return false;
        $.ajax({
            // url: 'https://ksapi.erdee.nl' + '/zipcode/' + escape(zip) + '/' + escape(num),
            url: 'https://ksapi.erdee.nl/v2/address/nl/' + escape(zip) + '/' + escape(num),
            headers: {
                'X-Origin-AuthToken' : 'nDCcJZAvk3vvRGZp4VDUGW7skZWgW9Vy'
            },
            type: 'get',
            dataType: "json"
        }).done(function (data) {
            if (typeof data.addresses[0].zipcode != "undefined") {
                zipField.val(data.addresses[0].zipcode.replace(zipRegex, '$1 $2'));
                strField.val(data.addresses[0].street);
                twnField.val(data.addresses[0].town.substring(0, 1).toUpperCase() + data.addresses[0].town.toLowerCase().slice(1));
                $(twnField).addClass('disabled');
                $(strField).addClass('disabled');
                $(twnField).parent().addClass('field--disabled');
                $(strField).parent().addClass('field--disabled');
            } else {
                $(twnField).removeClass('disabled');
                $(strField).removeClass('disabled');
                $(twnField).parent().removeClass('field--disabled');
                $(strField).parent().removeClass('field--disabled');
            }
        })
    }

    if (document.getElementById("zipcode")) {
        document.getElementById('zipcode').addEventListener('input', function (e) {
            e.target.value = e.target.value.replace(/[^\dA-Z]/gi, '').replace(/(.{4})/g, '$1 ').trim();
        });
    }

    if (document.getElementById("voorletters")) {
        document.getElementById('voorletters').addEventListener('input', function (e) {
            var input = e.target.value,
                sanitizedInput = input.replace(/[^\dA-Z]/gi, '').replace(/(.{1})/g, '$1.').trim(),
                isBackspace = e.inputType === 'deleteContentBackward';

            if (isBackspace && input.length < sanitizedInput.length) {
                e.target.value = input.slice(0, -2) + '.';
                if (e.target.value == '.') {
                    e.target.value = '';
                }
            } else {
                e.target.value = sanitizedInput;
            }
        });
    };

    // document.getElementById('telefoon').addEventListener('input', function (e) {
    //     e.target.value = e.target.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    // });

    // document.getElementById('street_number').addEventListener('input', function (e) {
    //     e.target.value = e.target.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    // });

    if (document.getElementById("telefoon")) {
        document.getElementById('telefoon').addEventListener('input', function (e) {
            maxLengthCheck(e.target);
        });
    }

    if (document.getElementById("street_number")) {
        document.getElementById('street_number').addEventListener('input', function (e) {
            maxLengthCheck(e.target);
        });
    }

    function maxLengthCheck(object) {

        if (isNaN(parseFloat(object.value))) {
            object.value = "";
        }
        if (object.value.length > object.maxLength) {
            object.value = object.value.slice(0, object.maxLength)
        }

        // console.log(object.maxLength, object.value.length, object.value)
    }

    $(function () {
        $(".send:not(.disabled)").on("click", validateForm);
    });

    function validateForm(e) {
        console.log($(this).parents().find('.contact-form')[0])
        var fieldContainer = $(this).parents().find('.contact-form')[0];

        var regFields = $(fieldContainer).find(".form-field-container[data-reg=true]");
        var requiredFields = $(fieldContainer).find(".form-field[data-required=true]");
        var result = true;

        $(regFields).each(function () {

            var regID = $(this).attr('field-id');

            $(this).find("input, textarea, select").removeClass("invalid");
            $(this).removeClass("form-field-container--invalid");

            var regexResult = true;

            var regexVal = $(this).attr('data-regex');
            var fieldVal = $(this).find('input')[0];
            var fieldValue = $(fieldVal).val();

            $(this).find(".invalid-error").hide();
 

            if (regexVal && fieldValue) {
                var regex = eval(regexVal.toString());
                var regexResult = regex.test(fieldValue);
            }

            if (!(regexResult === true)) {
                setTimeout(() => {
                    $(this).find("input, textarea, select").addClass("invalid");
                    $(this).find(".invalid-error").show();
                    $(this).addClass("form-field-container--invalid");
                    console.log($(this));
                }, 100)
                result = false;
            }
        })

        $(requiredFields).each(function () {
            $(this).find("input, textarea, select").removeClass("invalid");
            $(this).parent().removeClass("form-field-container--invalid");
            if (
                !($(this).find("input:not([type=radio], [type=checkbox]), textarea, select").val()) &&
                !($(this).find("input[type=radio]:checked, input[type=checkbox]:checked")[0])
            ) {
                $(this).find("input, textarea, select").addClass("invalid");
                $(this).parent().addClass("form-field-container--invalid");
                result = false;
            }
            var defaultDate = new Date('1975-01-01').toDateString(),
                checkValue = new Date($(this).find("input:not([type=radio], [type=checkbox]), textarea, select").val()).toDateString();
            // console.log({ defaultDate }, { checkValue })
            if (checkValue == defaultDate) {
                $(this).find("input, textarea, select").addClass("invalid");
                $(this).parent().addClass("form-field-container--invalid");
                result = false;
            }
        })

        if (result === true) {
            $(this).addClass('disabled');
        }

        // console.log({result})

        return result;

    }


    $.prototype.disableTab = function () {
        $(this).each(function () {
            $(this).attr('tabindex', '-1');
        });
    };

    setInterval(function () {
        $('.disabled').disableTab();
    }, 200);

    //OPTIN toggler
    var optinTogglers = document.querySelectorAll('.optin-toggler');
    if (typeof (optinTogglers) != 'undefined' && optinTogglers != null) {
        optinTogglers.forEach((optinToggler) => {
            $(optinToggler).on('click', function (event) {
                var optinWrapperId = event.currentTarget.getAttribute('data-toggler');
                var optinWrapper = $("#" + optinWrapperId);
                $(optinWrapper).toggle();
            });
        })
    };

    // OPTIN 
    var optins = document.querySelectorAll('.optin');
    if (typeof (optins) != 'undefined' && optins != null) {
        optins.forEach((optin) => {
            $(optin).on('click', function (event) {
                var optIN = optin.querySelectorAll('input')[0],
                    optOUT = optin.querySelectorAll('input')[1];

                // console.log(optIN.checked)
                if (optIN.checked == true) {
                    optOUT.checked = true;
                } else {
                    optIN.checked = true;
                }
                return false;
            });
        })
    };


} export {
    form
}