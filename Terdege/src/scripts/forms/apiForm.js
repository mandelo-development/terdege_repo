
import { scrollCont } from '../scrollContainer';
import { debounce } from '../functions';
import axios from '../../../../config/node_modules/axios';
var $ = require("../../../../config/node_modules/jquery");

async function form() {
    var forms = document.querySelectorAll('.contact-form');
    
    if (forms.length === 0) return;
    var form;

    $(document).on('change', 'form input#zipcode, form input#street_number', function () {
        form = $(this).closest('form');
        updateAddress(form);
    })

    function updateAddress(form) {
        var zipRegex = /^(\d{4})\s*([A-Z]{2})$/i;
        var numberRegex = /^(0|[1-9]\d*)$/;
        var zipField = $(form).find('input#zipcode');
        var numField = $(form).find('input#street_number');
        var strField = $(form).find('input#street');
        var twnField = $(form).find('input#town');
        var zip = zipField.val().replace(/(^\s+|\s+$)/g, '').replace(/ /g, '');
        var num = numField.val().replace(/(^\s+|\s+$)/g, '');
        if (!zip || !num || !zip.match(zipRegex) || !num.match(numberRegex))
            return false;
        $.ajax({
            // url: 'https://ksapi.erdee.nl' + '/zipcode/' + escape(zip) + '/' + escape(num),
            url: 'https://ksapi.erdee.nl/v2/address/nl/' + escape(zip) + '/' + escape(num),
            headers: {
                'X-Origin-AuthToken' : 'PlCLXmGC6WgR5ZEbC1gbIXhKaIH6Ww5T'
            },
            type: 'get',
            dataType: "json"
        }).done(function (data) {
            if (typeof data.addresses[0].zipcode != "undefined") {
                zipField.val(data.addresses[0].zipcode.replace(zipRegex, '$1 $2'));
                strField.val(data.addresses[0].street);
                twnField.val(data.addresses[0].town.substring(0, 1).toUpperCase() + data.addresses[0].town.toLowerCase().slice(1));
                $(twnField).addClass('disabled');
                $(strField).addClass('disabled');
                $(twnField).parent().addClass('field--disabled');
                $(strField).parent().addClass('field--disabled');
            } else {
                $(twnField).removeClass('disabled');
                $(strField).removeClass('disabled');
                $(twnField).parent().removeClass('field--disabled');
                $(strField).parent().removeClass('field--disabled');
            }
        })
    }

    if (document.getElementById("zipcode")) {
        document.getElementById('zipcode').addEventListener('input', function (e) {
            e.target.value = e.target.value.replace(/[^\dA-Z]/gi, '').replace(/(.{4})/g, '$1 ').trim();
        });
    }

    if (document.getElementById("voorletters")) {
        document.getElementById('voorletters').addEventListener('input', function (e) {
            var input = e.target.value,
                sanitizedInput = input.replace(/[^\dA-Z]/gi, '').replace(/(.{1})/g, '$1.').trim(),
                isBackspace = e.inputType === 'deleteContentBackward';

            if (isBackspace && input.length < sanitizedInput.length) {
                e.target.value = input.slice(0, -2) + '.';
                if (e.target.value == '.') {
                    e.target.value = '';
                }
            } else {
                e.target.value = sanitizedInput;
            }
        });
    };

    // document.getElementById('telefoon').addEventListener('input', function (e) {
    //     e.target.value = e.target.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    // });

    // document.getElementById('street_number').addEventListener('input', function (e) {
    //     e.target.value = e.target.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    // });

    if (document.getElementById("telefoon")) {
        document.getElementById('telefoon').addEventListener('input', function (e) {
            maxLengthCheck(e.target);
        });
    }

    if (document.getElementById("street_number")) {
        document.getElementById('street_number').addEventListener('input', function (e) {
            maxLengthCheck(e.target);
        });
    }

    function maxLengthCheck(object) {

        if (isNaN(parseFloat(object.value))) {
            object.value = "";
        }
        if (object.value.length > object.maxLength) {
            object.value = object.value.slice(0, object.maxLength)
        }

        // console.log(object.maxLength, object.value.length, object.value)
    }

    // $(function () {
    //     $(".send:not(.disabled)").on("click", validateForm);
    // });
    function validateForm(target) {
        var fieldContainer = $(target);
    
        var regFields = fieldContainer.find(".form-field-container[data-reg=true]");
        var requiredFields = fieldContainer.find(".form-field[data-required=true]");
        var result = true;
    
        regFields.each(function () {
            var $fieldContainer = $(this);
            var regID = $fieldContainer.attr('field-id');
            // console.log(regID);
            $fieldContainer.find("input, textarea, select").removeClass("invalid");
            $fieldContainer.removeClass("form-field-container--invalid");
    
            var regexResult = true;
    
            var regexVal = $fieldContainer.attr('data-regex');
            var fieldVal = $fieldContainer.find('input')[0];
            var fieldValue = $(fieldVal).val();
    
            $fieldContainer.find(".invalid-error").hide();
    
            if (regexVal && fieldValue) {
                var regex = eval(regexVal.toString());
                regexResult = regex.test(fieldValue);
            }
    
            if (!regexResult) {
                setTimeout(() => {
                    $fieldContainer.find("input, textarea, select").addClass("invalid");
                    $fieldContainer.find(".invalid-error").show();
                    $fieldContainer.addClass("form-field-container--invalid");
                }, 100);
                result = false;
            }
        });
    
        requiredFields.each(function () {
            var $field = $(this);
            var $fieldContainer = $field.parent();
    
            $field.find("input, textarea, select").removeClass("invalid");
            $fieldContainer.removeClass("form-field-container--invalid");
    
            if (
                !($field.find("input:not([type=radio], [type=checkbox]), textarea, select").val()) &&
                !($field.find("input[type=radio]:checked, input[type=checkbox]:checked")[0])
            ) {
                $field.find("input, textarea, select").addClass("invalid");
                $fieldContainer.addClass("form-field-container--invalid");
                result = false;
            }
    
            // .toDateString()
            var defaultDate = new Date().toLocaleDateString("nl-NL");
            var checkValue = new Date($field.find("input[type=date]").val()).toLocaleDateString("nl-NL");
            console.log(defaultDate.toString(), checkValue.toString())
            if (checkValue.toString() == defaultDate.toString()) {
                $field.find("input, textarea, select").addClass("invalid");
                $fieldContainer.addClass("form-field-container--invalid");
                result = false;
            }
        });
    
        if (result === true) {
            fieldContainer.addClass('disabled');
        }
    
        return result;
    }

    // function validateForm(target) {
    //     var fieldContainer = target;

    //     var regFields = $(fieldContainer).find(".form-field-container[data-reg=true]");
    //     var requiredFields = $(fieldContainer).find(".form-field[data-required=true]");
    //     var result = true;

    //     $(regFields).each(function () {

    //         var regID = $(this).attr('field-id');

    //         $(this).find("input, textarea, select").removeClass("invalid");
    //         $(this).removeClass("form-field-container--invalid");

    //         var regexResult = true;

    //         var regexVal = $(this).attr('data-regex');
    //         var fieldVal = $(this).find('input')[0];
    //         var fieldValue = $(fieldVal).val();

    //         $(this).find(".invalid-error").hide();
 

    //         if (regexVal && fieldValue) {
    //             var regex = eval(regexVal.toString());
    //             var regexResult = regex.test(fieldValue);
    //         }

    //         if (!(regexResult === true)) {
    //             setTimeout(() => {
    //                 $(this).find("input, textarea, select").addClass("invalid");
    //                 $(this).find(".invalid-error").show();
    //                 $(this).addClass("form-field-container--invalid");
    //                 // console.log($(this));
    //             }, 100)
    //             result = false;
    //         }
    //     })

    //     $(requiredFields).each(function () {
    //         $(this).find("input, textarea, select").removeClass("invalid");
    //         $(this).parent().removeClass("form-field-container--invalid");
    //         if (
    //             !($(this).find("input:not([type=radio], [type=checkbox]), textarea, select").val()) &&
    //             !($(this).find("input[type=radio]:checked, input[type=checkbox]:checked")[0])
    //         ) {
    //             $(this).find("input, textarea, select").addClass("invalid");
    //             $(this).parent().addClass("form-field-container--invalid");
    //             result = false;
    //         }
    //         var defaultDate = new Date('1975-01-01').toDateString(),
    //             checkValue = new Date($(this).find("input:not([type=radio], [type=checkbox]), textarea, select").val()).toDateString();
    //         // console.log({ defaultDate }, { checkValue })
    //         if (checkValue == defaultDate) {
    //             $(this).find("input, textarea, select").addClass("invalid");
    //             $(this).parent().addClass("form-field-container--invalid");
    //             result = false;
    //         }
    //     })

    //     if (result === true) {
    //         $(this).addClass('disabled');
    //     }

    //     // console.log({result})

    //     return result;

    // }


    $.prototype.disableTab = function () {
        $(this).each(function () {
            $(this).attr('tabindex', '-1');
        });
    };

    setInterval(function () {
        $('.disabled').disableTab();
    }, 200);

    //OPTIN toggler
    var optinTogglers = document.querySelectorAll('.optin-toggler');
    if (typeof (optinTogglers) != 'undefined' && optinTogglers != null) {
        optinTogglers.forEach((optinToggler) => {
            $(optinToggler).on('click', function (event) {
                var optinWrapperId = event.currentTarget.getAttribute('data-toggler');
                var optinWrapper = $("#" + optinWrapperId);
                $(optinWrapper).toggle();
            });


        })
    };

    // OPTIN 
    var optins = document.querySelectorAll('.optin');
    if (typeof (optins) != 'undefined' && optins != null) {
        optins.forEach((optin) => {
            $(optin).on('click', function (event) {
                var optIN = optin.querySelectorAll('input')[0],
                    optOUT = optin.querySelectorAll('input')[1];

                // console.log(optIN.checked)
                if (optIN.checked == true) {
                    optOUT.checked = true;
                } else {
                    optIN.checked = true;
                }
                return false;
            });
        })
    };

    // $(function () {
    //     $(".send:not(.disabled)").on("click", validateForm);
    // });

    
        var isFormValidated = false;
        document.querySelector(".contact-form").addEventListener("submit", function(e) {
            e.preventDefault();
            var form = document.querySelector(".contact-form");
            var loader = form.querySelector('.form-loader');

            // var validationState = validateForm(e.target);
            // console.log(validationState);

            if (!isFormValidated) {
                var validationState = validateForm(e.target);
                // console.log(validationState);
                
                // Update the validation state variable
                isFormValidated = validationState === true;
            }

            if(isFormValidated) {
                loader.style.display = 'block';
                var endpoint = form.getAttribute("api-endpoint");
                // console.log(endpoint.includes("ksapi_abonnement"))

                if (endpoint.includes("ksapi_abonnement") === true) {
                    // alert("axios abonnement call");
                    var xOriginToken;
                    var endpointUrl;
                    if (endpoint.includes("_dev") === true) {
                        endpointUrl = "https://t-ksapi.erdee.nl/v2/subscriptions";
                        xOriginToken = "PUitIB5ogScu7c0Rnot0ZCrwLgqVNeBB";
                    } else {
                        endpointUrl = "https://ksapi.erdee.nl/v2/subscriptions";
                        xOriginToken = "PlCLXmGC6WgR5ZEbC1gbIXhKaIH6Ww5T";
                    }

                    // console.log($(form)[0])

                    let formdata = new FormData(form);
                    var formDataObject = {};
                    formdata.forEach(function (value, key) {
                        // console.log({key}, {value})
                        var replacedKey = key.replace("[content]", "").replace("form_message[", "").replace("]", "").replace("form_message[", "").replace("[]", "")
                        formDataObject[replacedKey] = value;
                    });
                    
                    // console.log(formDataObject);

                    var genderInput = formDataObject['geslacht'],
                        intialsInput = formDataObject['voorletters'],
                        prepositionInput = formDataObject['tussenvoegsel'],
                        lastnameInput = formDataObject['achternaam'],
                        zipcodeInput = formDataObject['postcode'],
                        streetNumberInput = formDataObject['huisnummer'],
                        streetNumberExtraInput = formDataObject['toevoeging'],
                        streetInput = formDataObject['straat'],
                        townInput = formDataObject['woonplaats'],
                        emailInput = formDataObject['emailadres'],
                        birthdayInput = formDataObject['geboortedatum'],
                        phoneInput = formDataObject['telefoonnummer'],
                        ibanInput = formDataObject['iban'],
                        subscrTypeInput = formDataObject['subscr_type'],
                        subscrSrcInput = formDataObject['subscr_source'],
                        subscrUrlInput = formDataObject['subscr_utm_url'],
                        subscrPremiumCodeInput = formDataObject['subscr_premium_code'],
                        paymentInput = formDataObject['payment'],
                        startDateInput = formDataObject['ingangsdatum'];


                        /* consentAgreement */
                        class consentAgreement {
                            constructor(attrs) {
                                attrs.forEach((attr) => {
                                    var key = attr
                                        .getAttribute("name")
                                        .replace("[content]", "")
                                        .replace("form_message[", "")
                                        .replace("]", "")
                                        .replace("form_message[", "")
                                        .replace("[]", ""),
                                    value = attr.value;
                                    this[key] = value;
                                });
                            }
                        }

                        var agreement = new consentAgreement(
                            form.querySelectorAll(".optin input:checked")
                        );
                        
                    
                        var data = {
                            gender: genderInput,
                            initials: intialsInput,
                            middlename: prepositionInput,
                            lastname: lastnameInput,
                            email: emailInput,
                            phone: phoneInput,
                            birthday: birthdayInput,
                            address: {
                                street: streetInput,
                                street_number: streetNumberInput,
                                street_number_extra: streetNumberExtraInput,
                                zipcode: zipcodeInput,
                                town: townInput,
                            },
                            subscription: {
                                subscr_type: subscrTypeInput,
                                subscr_source: subscrSrcInput,
                                subscr_start: startDateInput,
                                payment_method: paymentInput,
                                bank_iban: ibanInput,
                                subscr_premium_code: subscrPremiumCodeInput,
                                subscription_free_fields: {
                                    subscr_utm_url: subscrUrlInput
                                }
                            }
                        }   
                        Object.assign(data, agreement);

                        console.log({data});

                    axios({
                        // method: 'post',
                        method: 'PUT',
                        url: endpointUrl,
                        // cors: true,
                        data: data,
                        headers: {
                            'X-Origin-AuthToken': xOriginToken,
                            'Content-Type': 'application/json',
                        },
                    }).then(response => {
                        // clearForm(form)
                        loader.style.display = 'none';

                        console.log({response});

                        var successMessage = response.data.user_message;
                        displayMessage(successMessage, true, form);
                        setTimeout(function() {
                            form.submit();  
                        }, 1000);
                    }).catch(error => {

                        // Check specific HTTP error status codes and log them
                        var httpStatus = error.response && error.response.status;
                        switch (httpStatus) {
                            case 401:
                            case 403:
                            case 405:
                            case 406:
                            case 415:
                            case 422:
                                if (error.response.data.debug !== undefined) {
                                    console.log("HTTP Error Status Code:", httpStatus, ":", error.response.data.error_code, ", " + error.response.data.debug);
                                } else {
                                    console.log("HTTP Error Status Code:", httpStatus, ":", error.response.data.error_code);
                                }
                                break;
                            default:
                                // Handle other HTTP status codes if needed
                                break;
                        }

                        loader.style.display = 'none';

                        console.log({error});
                        // console.log(error.response.data.user_message);
                        if (error.response && error.response.data && error.response.data.user_message) {
                            // Display the error message to the user
                            var errorMessage = error.response.data.user_message;
                            displayMessage(errorMessage, false, form);
                        } else {
                            // Handle other errors
                            // console.log("An error occurred:", error.message);
                            var errorMessage = "Er is iets misgegaan, neem contact op met de klantenservice.";
                            displayMessage(errorMessage, false, form);
                        }
                    })
                } else {
                    form.submit();
                }
            }
        });
    }
    
    

    //displayMessage van maken voor zowel error als succes handling (succes handling mits er geen redirect url is)

    function displayMessage(message, success, form) {
        var message = message.split('---- optional uuid ----')[0];
    
        var formMessage = form.querySelector('.form-notification');
        var formMessageText = formMessage.querySelector('.form-notification__text');
    
        formMessageText.textContent = message;
    
        if (success === true) {
            formMessage.classList.remove("form-notification--failed");
            formMessage.classList.add("form-notification--success");
            formMessage.style.display = "block";
        } else {
            formMessage.classList.remove("form-notification--success");
            formMessage.classList.add("form-notification--failed");
            formMessage.style.display = "block";
        }
    
} 

export {
    form
}