// import { DatePicker } from './datePicker';

// THROTTLE FUNCTION
export const throttle = function (func, limit) {
    let lastFunc;
    let lastRan;
    return function () {
        const context = this;
        const args = arguments;
        if (!lastRan) {
            func.apply(context, args);
            lastRan = Date.now();
        } else {
            clearTimeout(lastFunc);
            lastFunc = setTimeout(function () {
                if (Date.now() - lastRan >= limit) {
                    func.apply(context, args);
                    lastRan = Date.now();
                }
            }, limit - (Date.now() - lastRan));
        }
    };
};

// DEBOUNCE FUNCTION
export const debounce = function (func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

// SET ACTIVE
let dataPickerCount = 0;
export const setActive = (el, active) => {
    const formField = el.parentNode;
    if (formField.classList.contains('date')) {

        // if (active) {
        //     formField.classList.add('form-field--is-active');
        //     formField.classList.add('date-picker--filled');
        //     dataPickerCount++
        //     setTimeout(() => {
        //         if (dataPickerCount == 1) {
        //             DatePicker();
        //         }
        //     }, 300)
        // } else {
        //     formField.classList.remove('form-field--is-active');
        //     formField.classList.remove('date-picker--active');
            
        //     if (el.value === '') {
        //         formField.classList.remove('form-field--is-filled');
        //         formField.classList.remove('date-picker--filled');
        //     } else {
        //         formField.classList.add('form-field--is-filled');
        //         formField.classList.add('date-picker--filled');
        //     }
        // }
    } else {
        // if (document.querySelector('.date-selector') != null) {
        //     document.querySelector('.date-selector').style.maxHeight = null;
        // }
        
        if (active) {
            formField.classList.add('form-field--is-active');
        } else {
            formField.classList.remove('form-field--is-active');
            el.value === '' ?
                formField.classList.remove('form-field--is-filled') :
                formField.classList.add('form-field--is-filled');
        }
    }
};

export function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

export function menuAccordion(state) {
    var dropdownItems = document.querySelectorAll('.dropdown .nav-link');
    // console.log(dropdownItems);

    dropdownItems.forEach((dropdownItem) => {
        dropdownItem.addEventListener('click', (e) => {
            e.preventDefault();
            if (dropdownItem.nextElementSibling.nextElementSibling.style.maxHeight) {
                dropdownItem.classList.remove('active-dropdown');
                dropdownItem.nextElementSibling.nextElementSibling.style.maxHeight = null;
            } else {
                dropdownItem.classList.add('active-dropdown');
                dropdownItem.nextElementSibling.nextElementSibling.style.maxHeight = dropdownItem.nextElementSibling.nextElementSibling.scrollHeight + 'px';
            }
        })
    })

    // function accordionClick() {

    //     var acc = document.getElementsByClassName("question__item__question");
    //     var i;
    //     var j;

    //     for (i = 0; i < acc.length; i++) {
    //         acc[i].onclick = function() {

    //             if (this.classList.contains("active")) {

    //                 this.nextElementSibling.style.maxHeight = null;
    //                 this.classList.remove("active");

    //             } else {

    //                 for (j = 0; j < acc.length; j++) {
    //                     acc[j].nextElementSibling.style.maxHeight = null;
    //                     acc[j].classList.remove("active")
    //                 }

    //                 this.classList.add("active");

    //                 var panel = this.nextElementSibling;

    //                 if (panel.style.maxHeight) {
    //                     panel.style.maxHeight = null;
    //                 } else {
    //                     panel.style.maxHeight = panel.scrollHeight + "px";
    //                 }
                    
    //                 setTimeout(function() {
    //                     if (mediaQuery.matches) {
    //                         scrollCont.update(); 
    //                     }
    //                 }, 400);    
    //             }

    //         }
    //     }
    // }

    
    // accordionClick();
}