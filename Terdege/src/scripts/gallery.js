
import {scrollCont} from './scrollContainer';
var $ = require("../../../config/node_modules/jquery");

// Require jQuery (Fancybox dependency)
window.$ = window.jQuery = require('../../../config/node_modules/jquery');

// Fancybox
const fancybox = require('../../../config/node_modules/@fancyapps/fancybox');
// Fancybox Stylesheet
// const fancyboxCSS = require('../../../config/node_modules/!style!css!@fancyapps/fancybox/dist/jquery.fancybox.css');

async function gallery() {

    $( document ).ready(function() {
        resizeAllGridItems();
        $('.fancybox').fancybox();
    });

    window.addEventListener("resize", resizeAllGridItems);
    window.addEventListener("scroll", resizeAllGridItems);
    
    function resizeGridItem(item){
        var grid = document.getElementsByClassName("grid")[0];
        var rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
        var rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
        var rowSpan = Math.ceil((item.querySelector('picture').getBoundingClientRect().height+rowGap)/(rowHeight+rowGap));
        item.style.gridRowEnd = "span "+rowSpan;
        /*refresh();*/
    }
    
    function resizeAllGridItems(){
        var allItems = document.getElementsByClassName("foto_container");
        for(var x=0; x < allItems.length; x++){
            resizeGridItem(allItems[x]);
            scrollCont.update();
        }
    }
    
} export {
    gallery
}