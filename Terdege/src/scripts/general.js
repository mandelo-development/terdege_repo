import {scrollCont} from './scrollContainer';
// import { DatePicker } from './datePicker';
import {debounce, throttle, setActive, menuAccordion} from './functions';
import '../../../config/node_modules/bootstrap/js/dist/collapse';
var $ = require("../../../config/node_modules/jquery");

async function general() {
    $('.collapse').collapse()
    

    $(document).ready( function () {
        $('body').removeClass('no-transitions');
        $('.search__input').val('')
        if(parent.document.getElementById('config-bar')) {

            $('body').addClass('config-mode');
            scrollCont.destroy();

        } else {

        }

        if ($(window).width() <= 767) {

            scrollCont.destroy();

            $(window).scroll(function () {
                if ($(window).scrollTop() >= 40) {
                    $('.navigation').addClass('fixed-mob-header');
                } else {
                    $('.navigation').removeClass('fixed-mob-header');
                }

                didScrollMob = true;
            });

            // Hide Navbar on on scroll down
            var didScrollMob;
            var lastScrollTopMob = 0;
            var deltaMob = 50;
            var navbarMob = $('.navigation');
            var navbarHeightMob = $(navbarMob).outerHeight();

            setInterval(function() {
                if (didScrollMob) {
                    hasScrolledMob();
                    didScrollMob = false;
                }
            }, 250);

            function hasScrolledMob() {

                let stMob = $(this).scrollTop();

                // Make sure they scroll more than delta
                if(Math.abs(lastScrollTopMob - stMob) <= deltaMob)
                    return;

                if (navbarHeightMob > stMob) {
                    $(navbarMob).addClass('nav-top');
                } else {
                    $(navbarMob).removeClass('nav-top');
                }

                // If they scrolled down and are past the navbar, add class .nav-down.
                // This is necessary so you never see what is "behind" the navbar.
                if (stMob > lastScrollTopMob && stMob > navbarHeightMob) {
                    // Scroll Down
                    $(navbarMob).removeClass('nav-up').addClass('nav-down');
                } else {
                    // Scroll Up
                    if(stMob + $(window).height() < $(document).height()) {
                        $(navbarMob).removeClass('nav-down').addClass('nav-up');
                    }
                }

                lastScrollTopMob = stMob;
            }
    
            $('.navbar-toggler, .search-icon').click(throttle(function(event) {
                var nav = $('.navigation');
                var nav_word_reveal = $('.navigation .word-reveal');
    
                if (event.currentTarget.classList.contains('search-icon')) {
                    setTimeout(function() {
                        $('.search-wrapper .search__input').focus();
                    }, 420);
                }
    
                $(nav_word_reveal).toggleClass('revealed');
                $(nav).toggleClass('menu-open');
                $('body').toggleClass('lock-scroll');

                if ($(nav).hasClass('menu-open')) {
                    menuAccordion();
                }
            }, 1000));
    
            var section = $(".section-container");
    
            section.each(function (i, element) {
                var mobileSpaceAbove = parseInt($(element).attr('data-boven'));
                var mobileSpaceUnder = parseInt($(element).attr('data-onder'));
    
                if (isNaN(mobileSpaceAbove)) {
                    var padTop = parseInt($(element).css('padding-top'));
                    var newPadTop = padTop / 2;
    
                    $(element).css({'padding-top': newPadTop + 'px'});
                } else {
                    $(element).css({'padding-top': mobileSpaceAbove + 'px'});
                }
    
                if (isNaN(mobileSpaceUnder)) {
                    var padBot = parseInt($(element).css('padding-bottom'));
                    var newPadBot = padBot / 2;
    
                    $(element).css({'padding-bottom': newPadBot + 'px'});
                } else {
                    $(element).css({'padding-bottom': mobileSpaceUnder + 'px'});
                }
            });
    
        } else {
            $(".search-icon, .cross-icon").on('click', throttle(function (e) {
                var searchWrapper = $('.search-wrapper');
                var searchWrapperInput = $('.search-wrapper .search__input');
                var navigation = $('.navigation');

                $(navigation).toggleClass('icons-wrapper-up');
    
                if ($(searchWrapper).css('visibility') === 'hidden' ) {
                    $(searchWrapper).css('visibility','visible');
                    $(searchWrapper).css('opacity','1');
                    $(searchWrapperInput).focus();
                } else {
                    $(searchWrapper).css('visibility','hidden');
                    $(searchWrapper).css('opacity','0');
                }
            }, 1000));

            $('a[href*=\\#]').click(function(event) {
                if(parent.document.getElementById('config-bar') || event.currentTarget.classList.contains('diffuse-cookie-settings')) {
                    if (event.currentTarget.classList.contains('diffuse-cookie-settings')) {
                        
                    }
                } else {
                    event.preventDefault();
                    var hash = event.currentTarget.hash;
                    var hashSplit = hash.split("#");
                    var linkPoint = document.getElementById(hashSplit[1]);
                    scrollCont.scrollTo(linkPoint, -50, 1000)
                    scrollCont.update();
                }
            });

            // Hide Navbar on on scroll down
            var didScroll;
            var lastScrollTop = 0;
            var delta = 50;
            var navbar = $('.navigation');
            var navbarHeight = $(navbar).outerHeight();

            scrollCont.on('scroll', function (event) {
                didScroll = true;
            });

            setInterval(function() {
                if (didScroll) {
                    hasScrolled();
                    didScroll = false;
                }
            }, 250);

            function hasScrolled() {

                var offsetScroll = $('#js-scroll').offset().top;
                var st = Math.abs(offsetScroll);

                // Make sure they scroll more than delta
                if(Math.abs(lastScrollTop - st) <= delta)
                    return;

                if (navbarHeight > st) {
                    $(navbar).addClass('nav-top');
                } else {
                    $(navbar).removeClass('nav-top');
                }

                // If they scrolled down and are past the navbar, add class .nav-down.
                // This is necessary so you never see what is "behind" the navbar.
                if (st > lastScrollTop && st > navbarHeight){
                    // Scroll Down
                    $(navbar).removeClass('nav-up').addClass('nav-down');
                } else {
                    // Scroll Up
                    if(st + $(window).height() < $(document).height()) {
                        $(navbar).removeClass('nav-down').addClass('nav-up');
                    }
                }

                lastScrollTop = st;
            }
        }

        var accordions =  document.querySelectorAll('.accordion');
        if (typeof(accordions) != 'undefined' && accordions != null) {
            accordions.forEach((accordion) => {
                accordion.querySelector('.card button').click();
            });
        }
        

       
        /* BOLD PARAGRAPH */
        var parBold = $(".paragraph__bold");
    
        parBold.each(function (i, element) {
            var color = $(element).attr('bold__color');
            var boldWords = $(element).find('b');

            boldWords.each(function (i, word) {
                $(word).css('color', color);
            });
        });

        /* FORM LABELS */ 
        [].forEach.call(
            document.querySelectorAll('.form-field__label, .form-field__input, .form-field__textarea'),
            el => {
                el.onblur = (e) => {
                    setActive(el, false);
                };
                el.onfocus = (e) => {
                    setActive(el, true);
                };
                
            });
        
            
        // document.querySelectorAll('.form-field-container-date .form-field').forEach((el) => {
        //     let clickCount = 0;el;
        //     const dateSelector = el.closest('.form-field-container-date').querySelector('.date-selector');
        //     el.addEventListener('click', (e) => {
        //         if (clickCount === 0) {
        //             setTimeout(() => {
        //                 DatePicker();
        //             }, 400)
        //         }
        //         clickCount++;
        //         if (dateSelector.style.maxHeight) {
        //             dateSelector.style.maxHeight = null;
        //         } else {
        //             dateSelector.style.maxHeight = '224px';
        //         }
        //     });
        //     document.addEventListener('click', function(event) {
        //         var targetElement = event.target;
        //         var isClickInsideDiv = el.closest('.form-field-container-date').contains(targetElement) || targetElement === el.closest('.form-field-container-date');
              
        //         if (!isClickInsideDiv) {
        //             dateSelector.style.maxHeight = null;
        //         }
        //     })
        // });
          
        /*KENNISBANK DROPDOWN*/

        $('#artikel_thema_dropdown').on('change', debounce(function(event) {
            var catId = $(this).val(); // get selected value
            console.log(catId);
            if (catId) { // require a URL
                window.location = '/artikelen?data_object=article&categories=' + catId; // redirect
                console.log('/artikelen?data_object=article&categories=' + catId);
            }
            return false;
        }, 200));

    });


    // DISABLE PASTE SEARCH
    $('.search__input').bind('paste', function(){
        var self = this;
        setTimeout(function() {
            if(!/^[a-zA-Z]+$/.test($(self).val()))
                $(self).val('');
        }, 0);    
    });

    var searchInputs = document.getElementsByClassName('search__input');
    var searchAllInputs = Array.prototype.filter.call(searchInputs, function(input){
        input.addEventListener('input', function (e) {
            e.target.value = e.target.value.replace(/\(.+?\)/g, '').replace(/[^a-z0-9 ]+/gi, '');
            console.log(e.target.value)
        });
        input.addEventListener('paste', e => e.preventDefault());
        input.addEventListener('dragover', e => e.preventDefault());
    });

    $('.accordion .card').on('click', function(){
        if(!$(this).hasClass('active') ){  
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
        } else {
            $(this).removeClass('active'); 
        }    
    });
}

// SET ADS
async function adScript() {
    if($('.emgad').length > 0) {
        /* Start of Codalt Ad delivery tag - should be placed on every page with ads, in any place */
        document.addEventListener('DOMContentLoaded', function () {
            var emgAdTag = document.createElement('script');
            emgAdTag.type = 'text/javascript';
            emgAdTag.async = true;
            emgAdTag.src = 'https://adverteren.rd.nl/deliver.js';
            document.body.appendChild(emgAdTag);
        });
        /* End of Codalt Ad delivery tag */
    }
};

async function fileDrop() {
    [].forEach.call(
        document.querySelectorAll('.file-drop'),
        el => {
            var fileInput = el.parentNode.parentNode.parentNode.parentNode.querySelector('input');
            el.addEventListener("click", function(event){
                event.preventDefault();
                fileInput.click();    
            });

            fileInput.addEventListener("change", function(event){
                var fileInputText;
                if (fileInput.files.length > 1){
                    fileInputText = fileInput.files.length + ' bestanden geselecteerd'; 
                } else if (fileInput.files.length == 1) {    
                    fileInputText = fileInput.files.length + ' bestand geselecteerd'; 
                } else {
                    fileInputText = 'Plaats je bestanden hier'; 
                }

                el.querySelector('span').innerHTML = fileInputText;
            });
        }
    );
}


export {
    general, adScript, fileDrop
}