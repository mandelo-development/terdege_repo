import {
	getCookie,
	setCookie,
	checkCookie
} from './functions'

async function closeMessages() {
	message('popup_message', false, '')

	function message(name, resize, id) {
		if (document.querySelector('.' + name + '__close')) {
			let textHeight;
			if (resize) {
				setTextHeight(name)
				window.addEventListener('resize', function() {
					setTextHeight(name)
				})
			}
			document.querySelector('.' + name + '__close').addEventListener('click', function() {
				setCookie((name + id + '_closed'), true, 1);
				document.querySelector('.' + name).classList.add(name + '--closed');
				setTimeout(function() {
					document.querySelector('main').style.marginTop = document.querySelector('nav').offsetHeight + 'px';
				}, 200);
			})
			if (getCookie(name + id + '_closed') != 'true') {
				document.querySelector('.' + name).classList.remove(name + '--closed');
			}

			function setTextHeight(name) {
				textHeight = document.querySelector('.' + name + '__text').offsetHeight;
				document.querySelector('.' + name).style.maxHeight = textHeight + 'px';
				setTimeout(function() {
					document.querySelector('main').style.marginTop = document.querySelector('nav').offsetHeight + 'px';
				}, 200);
			}
		}

	}


}

export {
	closeMessages
}