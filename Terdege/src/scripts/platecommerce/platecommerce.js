import {
	PlateCommerce
} from './scripts/functions/PlateCommerce'
let {
	Cart,
	OrderHandler,
	Config,
	Order
} = PlateCommerce

import {
	pcCheckout,
	pcShipping,
	succesfullOrder
} from './scripts/pc-checkout'

import {
	pcEvents
} from './scripts/pc-events'

import {
	pcAddToCart,
	cartSize
} from './scripts/pc-cart'

import {
	pcTotal
} from './scripts/pc-total'

import {
	pcProduct
} from './scripts/pc-product'

import {
	pcFilters
} from './scripts/pc-filters'

import {
	range
} from './scripts/functions/range'

import {
	pcCoupons
} from './scripts/pc-coupons'

if (succesfullOrder()) {
	Cart.reset();
}

export function platecommerce() {
	Config.siteTranslationId = document.querySelector('body').dataset.site_translation_id;
	Config.siteTranslationLanguage = document.querySelector('body').dataset.site_translation_language;
	pcEvents();
	pcAddToCart(document);
	cartSize()
	pcTotal();
	pcProduct(document);
	pcFilters();
	pcCheckout();
	pcShipping();
	pcCoupons();

	if (document.querySelector('.filter__input__range__selector')) {
		range()
	}

}