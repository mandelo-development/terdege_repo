function devLog(message, name) {
	if (document.querySelector('body').dataset.dev == 'true') {
		if (name) {
			console.log(message, name);
		} else {
			console.log(message);
		}
	}
}
export {
	devLog
}