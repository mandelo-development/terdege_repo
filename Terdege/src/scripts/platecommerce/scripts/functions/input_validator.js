// function inputValidator(input) {
// 	var validInput = false;
// 	if (input.value) {
// 		if (input.type == "email" && !validateEmail(input.value)) {
// 			input.classList.add('pc--error');
// 			validInput = false;
// 			// return false;
// 		} else if (input.type == "tel" && validatePhone(input.value)) {
// 			input.classList.add('pc--error');
// 			// return false;
// 			validInput = false;
// 		} else if ((input.type == "checkbox" || input.type == "radio") && input.required) {
// 			console.log(input.name)
// 			if (document.querySelector('input[name="' + input.name + '"]:checked')) {
// 				input.classList.remove('pc--error');
// 				input.closest('.form-field').classList.remove('pc--field-error');
// 				validInput = true;
// 				// return true;
// 			} else {
// 				input.classList.add('pc--error');
// 				input.closest('.form-field').classList.add('pc--field-error');
// 				validInput = false;
// 				// return false;
// 			}
// 		} else {
// 			input.classList.remove('pc--error');
// 			// return true;
// 			validInput = true;
// 		}
// 	} else if (input.required) {
// 		console.log({1: input})
// 		// return false;
// 		validInput = false;
// 	} else {
// 		input.classList.remove('pc--error');
// 		// return true;
// 		validInput = true;
// 	}

// 	console.log(input.closest('form').querySelector('.error-message-checkout ul'))
// 	return validInput;
// }

function inputValidator(input) {
  var errorMessage;
  var validInput = false;
  var validValue;

  if (input.getAttribute("placeholder") !== null) {
    validValue = input.getAttribute("placeholder");
  } else {
    validValue = input.name;
  }

  var errorMessage = "Het veld " + validValue + " is verplicht";
  //   console.log(errorMessage, input.getAttribute("placeholder"));
  if (input.value !== "") {
    if (input.type == "email" && !validateEmail(input.value)) {
      input.classList.add("pc--error");
      validInput = false;
      // return false;
      addErrorMessage(input, "Ongeldig e-mailadres");
    } else if (input.type == "tel" && validatePhone(input.value)) {
      input.classList.add("pc--error");
      // return false;
      validInput = false;
      addErrorMessage(input, "Ongeldig telefoonnummer");
    } else if (
      (input.type == "checkbox" || input.type == "radio") &&
      input.required
    ) {
      console.log(input.name);
      if (document.querySelector('input[name="' + input.name + '"]:checked')) {
        input.classList.remove("pc--error");
        input.closest(".form-field").classList.remove("pc--field-error");
        validInput = true;
        // return true;
        removeErrorMessage(input, errorMessage);
      } else {
        input.classList.add("pc--error");
        input.closest(".form-field").classList.add("pc--field-error");
        validInput = false;
        // return false;
        addErrorMessage(input, errorMessage);
      }
    } else {
      input.classList.remove("pc--error");
      // return true;
      validInput = true;
      removeErrorMessage(input, errorMessage);
    }
  } else if (input.required) {
    console.log({ validInput }, input);
    // return false;
    input.classList.add("pc--error");
    validInput = false;
    addErrorMessage(input, errorMessage);
  } else {
    input.classList.remove("pc--error");
    // return true;
    validInput = true;
    removeErrorMessage(input, errorMessage);
  }

  return validInput;
}

function addErrorMessage(input, message) {
  var errorList = input
    .closest("form")
    .querySelector(".error-message-checkout ul");

  var existingError = Array.from(errorList.children).find(function (item) {
    return item.textContent.trim() === message.trim();
  });

  if (!existingError) {
    var errorMessageLi = document.createElement("li");
    errorMessageLi.textContent = message;
    errorList.appendChild(errorMessageLi);
  } else {
    var errorMessageLi = "";
  }
}

function removeErrorMessage(input, message) {
  var errorList = input
    .closest("form")
    .querySelector(".error-message-checkout ul");
  var errorMessages = errorList.querySelectorAll("li");
  errorMessages.forEach(function (errorMsg) {
    if (errorMsg.textContent === message) {
      errorList.removeChild(errorMsg);
    }
  });
}

function validateEmail(email) {
  const re =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validatePhone(phone) {
  const re = /^[a-zA-Z]+$/;
  return re.test(phone);
}
export { inputValidator };
