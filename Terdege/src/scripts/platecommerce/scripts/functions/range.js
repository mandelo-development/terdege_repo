async function range() {
	let range = document.querySelectorAll('.filter__input__range');
	range.forEach((rangeFilter, i) => {
		let rangeMin = rangeFilter.querySelector('.filter__input__range__selector__min');
		let rangeMax = rangeFilter.querySelector('.filter__input__range__selector__max');
		let valueMin = rangeFilter.querySelector('.filter__input__range__values__min');
		let valueMax = rangeFilter.querySelector('.filter__input__range__values__max');
		let inputMin = rangeFilter.querySelector('.filter__input__range__bar__min');
		let inputMax = rangeFilter.querySelector('.filter__input__range__bar__max');
		let inputRange = rangeFilter.querySelector('.filter__input__range__bar__range');
		let minValue = rangeMin.value;
		let maxValue = rangeMax.value;
		let min = parseInt(rangeMin.min);
		let max = parseInt(rangeMax.max);
		let diff = max - min;
		let minPercent = 0;
		let maxPercent = 100;

		// executeChange()
		rangeMin.addEventListener('input', executeChange);
		rangeMax.addEventListener('input', executeChange);
		rangeMin.addEventListener('change', executeChange);
		rangeMax.addEventListener('change', executeChange);
		valueMin.addEventListener('change', executeChangeTyped);
		valueMax.addEventListener('change', executeChangeTyped);

		rangeFilter.addEventListener('mousemove', function(event) {
			var x = event.clientX - this.offsetLeft;
			var relativeX = (x / this.offsetWidth) * 100;
			var xDiffMin = Math.abs(minPercent - relativeX);
			var yDiffMin = Math.abs(maxPercent - relativeX);
			if (xDiffMin >= yDiffMin) {
				rangeMax.classList.add('filter__input__range__selector--active');
				rangeMin.classList.remove('filter__input__range__selector--active');
			} else {
				rangeMax.classList.remove('filter__input__range__selector--active');
				rangeMin.classList.add('filter__input__range__selector--active');

			}
		})
		executeChange();

		function executeChangeTyped() {
			minValue = valueMin.value;
			maxValue = valueMax.value;
			if (minValue < min) {
				minValue = min;
				valueMin.value = min;
			}
			if (maxValue > max) {
				maxValue = max;
				valueMax.value = max;
			}
			processValues()
		}

		function executeChange() {
			minValue = rangeMin.value;
			maxValue = rangeMax.value;
			processValues()
		}

		function processValues() {
			minPercent = Math.round(((minValue - min) / diff) * 100);
			maxPercent = Math.round(((maxValue - min) / diff) * 100);
			valueMin.value = minValue;
			valueMax.value = maxValue;
			inputMin.style.left = minPercent + '%';
			inputMax.style.left = maxPercent + '%';
			inputRange.style.left = minPercent + '%';
			inputRange.style.width = (maxPercent - minPercent) + '%';
		}
	});
}

export {
	range
}