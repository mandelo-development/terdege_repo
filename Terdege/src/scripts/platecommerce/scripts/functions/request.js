import axios from '../../../../../../config/node_modules/axios';
let lang = document.getElementsByTagName('html')[0].getAttribute('lang') + '/';
if (!window.location.href.includes('/' + lang)) {
	lang = '';
}
async function xhrRequest(path, data, state) {
	return new Promise(resolve => {
		axios.post('/' + lang + path, data)
			.then(function(response) {
				resolve(response.data);
			})
			.catch(function(error) {
				console.log("Error, er ging iets mis. Neem contact op met de beheerder van deze website", error);
			});
		if (state) {
			let i, text = []
			for (i in data) {
				if (data.hasOwnProperty(i)) {
					if ((data[i].length > 0 || i == 'page') && encodeURIComponent(data[i]) != "") {
						text.push(i + "=" + encodeURIComponent(data[i]));
					}
				}
			}
			text = text.join("&").split(' ').join('_');
			window.history.pushState(null, null, "?" + text);
		}
	});
}
export {
	xhrRequest
}
