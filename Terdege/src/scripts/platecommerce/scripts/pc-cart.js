import {
	PlateCommerce
} from './functions/PlateCommerce'
import {
	xhrRequest
} from './functions/request'
import {
	debounce
} from './functions/debounce'
import {
	devLog
} from './functions/devlog'
import {
	getCombination
} from './pc-product'

let {
	Cart,
	OrderHandler,
	Config
} = PlateCommerce
let currentButton;
let body = document.querySelector('body')

// Create events
const cartChanged = new Event('cartChanged');
const cartReady = new Event('cartReady');

// Add product to cart
async function pcAddToCart(element, noupdate) {
	let addButtons = element.querySelectorAll('.pc__product__add-to-cart');
	addButtons.forEach((button, i) => {
		button.addEventListener('click', function() {
			this.disabled = true;
			let productId = this.dataset.ecwid;
			let productElement = document.querySelector('#pc__product' + productId);
			let options = getProductOptions(productElement);
			productModifier(this, options, "productAdded")
		})
	});
	if (!noupdate) {
		updateCart()
	}
}

function getProductOptions(productElement, simple) {
	let productQuantity = 1;
	if (productElement.querySelector('.pc__product__quantity-input')) {
		productQuantity = productElement.querySelector('.pc__product__quantity-input').value;
	}
	let optionSelects = productElement.querySelectorAll('.pc__product__variations__option');
	let options = {};
	let productVariationId;
	optionSelects.forEach((option, i) => {
		if (option.type == 'radio') {
			options[option.name] = productElement.querySelector('input[name="' + option.name + '"]:checked').value;
		} else if (option.type == 'checkbox') {
			options[option.name] = [];
			productElement.querySelectorAll('input[name="' + option.name + '"]:checked').forEach((check, i) => {
				options[option.name].push(check.value);
			});
		} else {
			options[option.name] = option.value;
		}
	});
	if (parseInt(productElement.dataset.variation_id) > 0) {
		productVariationId = parseInt(productElement.dataset.variation_id);
	}
	let addedProduct;
	if (simple) {
		return new Product(false, productQuantity, options, productVariationId)
	} else {
		return new Product(productElement.dataset.ecwid, productQuantity, options, productVariationId)
	}
}

// Delete product from cart
function pcRemoveFromCart(element) {
	let removeButtons = element.querySelectorAll('.pc__cart__list__item__delete');
	removeButtons.forEach((button, i) => {
		button.addEventListener('click', function() {
			productModifier(this, button.dataset.uuid, "productRemoved")
		})
	});
}


// Change products in cart
function pcChangeInCart(element) {
	element.querySelectorAll('.pc__cart__list__item').forEach((item, j) => {
		let changeInput = item.querySelector('.pc__product__quantity-input');
		let changeSelects = item.querySelectorAll('.pc__product__variations__option');
		let timer;
		if (changeInput) {
			changeInput.addEventListener('keyup', function() {
				clearTimeout(timer);
				timer = setTimeout(function() {
					changeExecutor(item, changeInput);
				}, 500)
			})
			changeInput.addEventListener('change', function() {
				changeExecutor(item, changeInput);
			})
		}

		changeSelects.forEach((input, i) => {
			input.addEventListener('change', function() {
				changeExecutor(item, input);
			})
		});

		function changeExecutor(item, input) {
			let options = {};
			options = getOptions(changeSelects, item);

			console.log(options);

			let variation = false;
			if (item.dataset.combinations) {
				variation = getCombination(options, item.dataset.combinations).ecwidid;
			}
			let changeParameters = {}
			if (variation) {
				changeParameters['productVariationId'] = variation;
				if (changeSelects[0]) {
					changeParameters['options'] = {};
				}
			} else {
				if (changeSelects[0]) {
					changeParameters['options'] = options;
				}
				changeParameters['productVariationId'] = '';
			}
			let quantityFilled = parseInt(item.querySelector('.pc__product__quantity-input').value);
			let quantityMax = parseInt(item.querySelector('.pc__product__quantity-input').max)
			if (quantityFilled > quantityMax) {
				quantityFilled = quantityMax;
			}
			changeParameters['quantity'] = quantityFilled;
			productModifier(input, input.dataset.uuid, "productChanged", changeParameters)
		}
	});

}

function getOptions(changeSelects, item) {
	let options = {};
	changeSelects.forEach((option, k) => {
		if (option.type == 'radio') {
			options[option.name] = item.querySelector('input[name="' + option.name + '"]:checked').value;
		} else if (option.type == 'checkbox') {
			options[option.name] = [];
			item.querySelectorAll('input[name="' + option.name + '"]:checked').forEach((check, i) => {
				options[option.name].push(check.value);
			});
		} else {
			options[option.name] = option.value;
		}
	});
	return options;
}

// Modifier handler
function productModifier(element, action, event, options) {
	element.disabled = true;
	let product;
	async function modifyProduct() {
		if (event == "productAdded") {
			devLog('Selected options to add', action)
			product = await Cart.addItem(action);
		} else if (event == "productChanged") {
			devLog('Selected options to change', options)
			product = await Cart.updateItem(action, options);
		} else if (event == "productRemoved") {
			product = await Cart.deleteItem(action);
		}
	}
	modifyProduct().then(function() {
		let eventProduct = new CustomEvent(event, {
			"detail": product
		});
		element.disabled = false;
		document.dispatchEvent(eventProduct);
		document.dispatchEvent(cartChanged);
	})
}

// Reload all the carts in the page
function updateCart() {
	let cartElements = document.querySelectorAll('.pc__cart');
	cartElements.forEach((cart, i) => {
		cart.classList.add('pc__cart--loading')
	});
	if (cartElements.length > 0) {
		let cartInfo = {},
			result
		OrderHandler.computeOrderFromCart().then((computation) => {
			cartInfo['cart_json'] = encodeURI(JSON.stringify(computation.items))
			async function request() {
				result = await xhrRequest('platecommerce', cartInfo, false);
			}
			request().then(function() {
				cartElements.forEach((cart, i) => {
					cart.classList.remove('pc__cart--loading')
					cart.innerHTML = result;
					pcRemoveFromCart(cart)
					pcChangeInCart(cart)
				});
				document.dispatchEvent(cartReady);
			})
		}).catch((error) => {
			console.log("Something went wrong, contact the owner of this website.", JSON.stringify(error.response))
		})
	} else {
		cartSize()
	}
}

// Size of cart
function cartSize() {
	let length = Cart.getItems().length;
	let quantity = 0;
	Cart.getItems().forEach((cartitem, i) => {
		quantity += cartitem.quantity
	});
	// Show amount in Cart and body tag
	document.querySelectorAll('.pc__cartsize').forEach((cartsize, i) => {
		if (cartsize.dataset.type == 'quantity') {
			let quantityAmount
			cartsize.innerHTML = quantity
			body.dataset.cartsize = quantity
		} else {
			cartsize.innerHTML = length
			body.dataset.cartsize = length
		}
	});
}

// Product template
function Product(productId, quantity, options, productVariationId) {
	if (productId) {
		this.id = parseInt(productId);
	}
	if (quantity) {
		this.quantity = parseInt(quantity);
	}
	if (productVariationId) {
		this.productVariationId = productVariationId;
	} else {
		this.selectedOptions = options;
	}
}

function ProductOption(name, value, priceModifier, priceModifierType) {
	this.name = name;
	this.value = value;
	this.priceModifier = parseFloat(priceModifier);
	this.priceModifierType = priceModifierType
}

export {
	pcAddToCart,
	updateCart,
	pcRemoveFromCart,
	getProductOptions,
	cartSize
}