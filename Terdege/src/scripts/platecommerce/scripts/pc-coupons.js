import {
	xhrRequest
} from './functions/request';
import {
	PlateCommerce
} from './functions/PlateCommerce'
let {
	Cart,
	OrderHandler,
	Config
} = PlateCommerce
let couponListElementRemove = '<button>-</button>';

const cartChanged = new Event('cartChanged');



// Load coupons and add eventlisteners
async function pcCoupons() {

	document.querySelectorAll('.pc__coupons').forEach((couponList, i) => {
		updateCouponList();
		couponList.querySelector('form').addEventListener('submit', function(event) {
			event.preventDefault();
			addCoupon(this, true)
		})
	});


	document.querySelectorAll('.pc__coupon').forEach((couponList, i) => {
		let couponInput = couponList.querySelector('input');
		OrderHandler.computeOrderFromCart().then((computation) => {
			if (computation.discountCoupons[0]) {
				/* CUSTOM FOR TERDEGE */
				if (localStorage.getItem('member_coupon') != computation.discountCoupons[0].code) {
					couponList.querySelector('form input').value = computation.discountCoupons[0].code;
					couponInput.classList.add('pc__coupons__add-coupon--success')
				}
				/* ENDCUSTOM FOR TERDEGE */
			}
		})
		couponList.querySelector('form').addEventListener('submit', function(event) {
			event.preventDefault();
			addCoupon(this)
		})
	});
}

// Add coupon and handle response

function addCoupon(thisForm, list) {
	/* CUSTOM FOR TERDEGE */
	if (!list) {
		OrderHandler.computeOrderFromCart().then((computation) => {
			computation.discountCoupons.forEach((coupon, i) => {
				if (coupon.code != localStorage.getItem('member_coupon')) {
					Cart.deleteDiscountCouponCode(coupon.code)
				}
			});
		})
	}
	/* ENDCUSTOM FOR TERDEGE */
	let couponInput = thisForm.querySelector('input');
	couponInput.classList.remove('pc__coupons__add-coupon--error')
	couponInput.classList.remove('pc__coupons__add-coupon--success')
	couponInput.classList.add('pc__coupons__add-coupon--loading')
	let couponData = couponInput.value.trim();
	let couponAdd = Cart.addDiscountCouponCode(couponData);
	OrderHandler.computeOrderFromCart().then((computation) => {
		if (computation.discountCoupons.filter(e => e.code === couponData).length > 0) {
			couponInput.classList.add('pc__coupons__add-coupon--success')
			document.dispatchEvent(cartChanged);
			couponInput.dataset.status = 'coupon_added';
			if (list) {
				updateCouponList(computation.discountCoupons)
			}
			let couponAdded = new CustomEvent('couponAdded', {
				"detail": {
					"coupon": couponData,
					"status": "coupon_added",
					"computation": computation
				}
			});
			document.dispatchEvent(couponAdded);
			thisForm.dispatchEvent(couponAdded);
		} else {
			console.log(computation);
			let ignoredCoupon = computation.ignoredDiscountCoupons.filter(e => e.code === couponData)[0];
			// Possible response reasons are ["not_found", "subtotal_too_low", "missing_applicable_products"]
			couponInput.dataset.status = ignoredCoupon.reason;
			couponInput.classList.add('pc__coupons__add-coupon--error')
			Cart.deleteDiscountCouponCode(couponData)
			let couponIgnored = new CustomEvent('couponIgnored', {
				"detail": {
					"coupon": couponData,
					"status": ignoredCoupon.reason,
					"computation": computation
				}
			});
			document.dispatchEvent(couponIgnored);
			thisForm.dispatchEvent(couponIgnored);
			/* CUSTOM FOR TERDEGE */
			document.dispatchEvent(cartChanged);
			/* ENDCUSTOM FOR TERDEGE */
		}
		couponInput.classList.remove('pc__coupons__add-coupon--loading')
	})
}

// Update lijst
function updateCouponList(couponsInput) {
	if (couponsInput) {
		executeUpdateCoupon(couponsInput)
	} else {
		OrderHandler.computeOrderFromCart().then((computation) => {
			let couponsInput = computation.discountCoupons
			executeUpdateCoupon(couponsInput)
		})
	}

	function executeUpdateCoupon(couponsInput) {
		document.dispatchEvent(cartChanged);
		document.querySelectorAll('.pc__coupons').forEach((couponList, i) => {
			couponList.querySelector('ul').innerHTML = '';
			couponsInput.forEach((code, i) => {
				let couponListElement = document.createElement("LI");
				couponListElement.innerHTML = '<span>' + code.code + '</span>' + couponListElementRemove;
				couponList.querySelector('ul').appendChild(couponListElement)
			});
			couponList.querySelectorAll('li button').forEach((button, i) => {
				button.addEventListener('click', function() {
					Cart.deleteDiscountCouponCode(this.parentElement.querySelector('span').innerHTML);
					updateCouponList();
				})
			});
		})
	}
}


export {
	pcCoupons
}