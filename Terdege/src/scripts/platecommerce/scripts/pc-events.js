import {
	PlateCommerce
} from './functions/PlateCommerce'
let {
	Cart,
	OrderHandler,
	Config
} = PlateCommerce

import {
	devLog
} from './functions/devlog'

import {
	updateCart,
	cartSize
} from './pc-cart'
import {
	pcShipping
} from './pc-checkout'

import {
	pcTotal
} from './pc-total'
let pcEventListeners = [{
	"name": "added",
	"event": 'productAdded'
}, {
	"name": "changed",
	"event": 'productChanged'
}, {
	"name": "removed",
	"event": 'productRemoved'
}]

function pcEvents() {
	pcEventListeners.forEach((listener, i) => {
		document.addEventListener(listener.event, function(e) {
			devLog(('EVENT - Product is ' + listener.name), {
				details: e.detail,
				eventName: listener.event
			})
		})
	});
	document.addEventListener('totalChanged', function() {
		pcTotal();
	})
	document.addEventListener('cartChanged', function() {
		devLog('Cart is changed', {
			details: Cart.getItems(),
			eventName: 'cartChanged'
		});
		updateCart();
		pcTotal();
		pcShipping();
		cartSize()
		document.querySelectorAll('.pc__checkout__to_payment_provider__button').forEach((button, i) => {
			if (Cart.getItems().length > 0) {
				button.disabled = false;
			} else {
				button.disabled = true;
			}
		});
	})
	document.addEventListener('orderSuccesfull', function(e) {
		devLog("Succesfull Order", {
			details: e.detail,
			eventName: 'orderSuccesfull'
		});
	})
	document.addEventListener('orderNotSuccesfull', function(e) {
		console.log("Er ging iets mis met uw bestelling, neem contact op met de beheerder van deze website.");
		devLog("No succesfull Order", {
			details: e.detail,
			eventName: 'orderNotSuccesfull'
		})
	})
	document.addEventListener('cartReady', function() {
		devLog('Cart is ready', {
			eventName: 'cartReady'
		});
	})
	document.addEventListener('couponAdded', function(e) {
		devLog("Coupon " + e.detail.coupon + " succesfully added", {
			details: e.detail,
			eventName: 'couponAdded'
		});
	})
	document.addEventListener('couponIgnored', function(e) {
		devLog("Coupon " + e.detail.coupon + " not added", {
			details: e.detail,
			eventName: 'couponIgnored'
		});
	})
}


export {
	pcEvents
}