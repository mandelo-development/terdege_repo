import {
	xhrRequest
} from './functions/request'
import {
	range
} from './functions/range'
import {
	elementInViewport
} from './functions/elementinviewport'
import {
	pcAddToCart,
	pcRemoveFromCart
}
from './pc-cart'
import {
	pcProduct
} from './pc-product'

let resultElement = document.querySelector('.pc__index__result');
// Base array with all parameters
let filterParameters = {};
let filtering = false;
let pageEnd = false;
let sortSelector = document.querySelector('.pc__index__result__sort');
let prevParameters;
filterParameters['page'] = 1;

async function pcFilters() {
	if (document.querySelector('.pc__index__filters')) {
		if (sortSelector) {
			filterParameters['sort_type'] = sortSelector.value;
			// Change sort option
			sortSelector.addEventListener('change', function() {
				filterParameters['sort_type'] = sortSelector.value;
				filterParameters['page'] = 1;
				updateResult(filterParameters)
			})
		}

		// Check Attrutes filters, add to filterParameters if checked
		filterParameters['category'] = document.querySelector('.pc__index').dataset.id;
		let attributeNames = document.querySelectorAll('.pc__index__filters__attributes');
		if (attributeNames.length > 0) {
			filterParameters['attributenames'] = attributeNames[0].dataset.attributenames.split('|')
			let allFilterInputs = document.querySelectorAll('.pc__index__filters__tax__items__item__input');
			allFilterInputs.forEach((input, i) => {
				filterParameters[input.dataset.parent] = [];
				checkIfChecked(input);
				input.addEventListener('change', function() {
					filterParameters['page'] = 1;
					let thisInput = this;
					checkIfChecked(thisInput);
					updateResult(filterParameters)
				})
			});

		}

		function checkIfChecked(thisInput) {
			if (thisInput.checked) {
				filterParameters[thisInput.dataset.parent].push(thisInput.dataset.id);
			} else {
				const index = filterParameters[thisInput.dataset.parent].indexOf(thisInput.dataset.id);
				if (index > -1) {
					filterParameters[thisInput.dataset.parent].splice(index, 1);
				}
			}
		}

		// Pricefilter
		let priceFilter = document.querySelector('.pc__index__filters__price');
		if (priceFilter) {
			priceFilter.querySelectorAll('input').forEach((rangeInput, i) => {
				rangeInput.addEventListener('change', function() {
					filterParameters['page'] = 1;
					if (priceFilter.querySelector('.filter__input__range__values__min')) {
						filterParameters['price_min'] = priceFilter.querySelector('.filter__input__range__values__min').value;
						filterParameters['price_max'] = priceFilter.querySelector('.filter__input__range__values__max').value;
					} else {
						filterParameters['price_min'] = priceFilter.querySelector('.filter__input__range__selector__min').value;
						filterParameters['price_max'] = priceFilter.querySelector('.filter__input__range__selector__max').value;
					}
					updateResult(filterParameters);
				})
			});
		}

		pagination()
	}
}

// Pagination
function pagination() {
	let pageLinks = resultElement.querySelectorAll('.pagination__item');
	if (pageLinks[0]) {
		pageLinks.forEach((page, i) => {
			page.addEventListener('click', function() {
				filterParameters['page'] = this.dataset.targetindex;
				updateResult(filterParameters);
			})
		});
	} else if (resultElement.dataset.pagination == 'lazy') {
		document.addEventListener('scroll', function() {
			let resultProducts = resultElement.querySelectorAll(".pc__product");
			let lastProduct = resultProducts[resultProducts.length - 1];
			if (elementInViewport(lastProduct, 100) && !filtering && !pageEnd) {
				filtering = true;
				filterParameters['page'] = filterParameters['page'] + 1;
				updateResult(filterParameters, true);
			}
		})
	}
}

// Function that updates result
function updateResult(parameters, append) {
	resultElement.classList.add('pc__index__result--loading');
	let result;
	async function request() {
		if (resultElement.dataset.url_parameters == "true") {
			result = await xhrRequest('platecommerce', parameters, true);
		} else {
			result = await xhrRequest('platecommerce', parameters, false);
		}
	}
	request().then(function() {
		if (result.includes('THE-END-PAGE')) {
			pageEnd = true;
		} else {
			pageEnd = false;
		}
		if (append) {
			resultElement.innerHTML = resultElement.innerHTML + result;
		} else {
			resultElement.innerHTML = result;
		}
		resultElement.classList.remove('pc__index__result--loading');
		pagination();
		pcAddToCart(resultElement, true);
		pcRemoveFromCart(resultElement);
		pcProduct(resultElement)
		filtering = false;
		prevParameters = parameters
	})
}

export {
	pcFilters
}