import {
	xhrRequest
} from './functions/request';
import {
	getProductOptions
} from './pc-cart'
let currentCombination = false;
import {
	PlateCommerce
} from './functions/PlateCommerce'
import {
	devLog
} from './functions/devlog'
let {
	OrderHandler,
	Order
} = PlateCommerce

function pcProduct(element) {
	element.querySelectorAll('.pc__product__variations__option').forEach((option, i) => {
		let productElement = document.querySelector('#pc__product' + option.dataset.ecwid);
		let thisProduct = JSON.parse(decodeURIComponent(productElement.dataset.product));
		let priceElement = productElement.querySelector('.pc__product__price');
		let compareToPriceElement = productElement.querySelector('.pc__product__comparetoprice');
		let quantityInput = productElement.querySelector('.pc__product__quantity-input');
		let quantityOutput = productElement.querySelector('.pc__product__quantity');
		let addToCartButton = productElement.querySelector('.pc__product__add-to-cart');
		let skuOutput = productElement.querySelector('.pc__product__sku');
		let currentImages = false;
		// changeOption(option, false)

		// Change price onchange of options
		option.addEventListener('change', function() {
			changeOption(option, true)
			updatePrice(productElement)
			quantityCheck()
		})

		let timer;
		if (quantityInput) {
			quantityInput.addEventListener('keyup', function() {
				clearTimeout(timer);
				timer = setTimeout(function() {
					updatePrice(productElement)
					quantityCheck()
				}, 500)
			})
		}

		function quantityCheck() {
			if (quantityInput) {
				if (parseInt(quantityInput.value) > parseInt(quantityInput.max)) {
					addToCartButton.disabled = true;
				} else {
					addToCartButton.disabled = false;
				}
			} else {
				addToCartButton.disabled = false;
			}
		}

		function updatePrice(productElement) {
			priceElement.classList.add('pc__product__price--loading')
			compareToPriceElement.classList.add('pc__product__comparetoprice--loading')
			let order = new Order([getProductOptions(productElement)])
			OrderHandler.computeOrder(order).then((computation) => {
				let computedItem = computation.items[0];
				devLog("Computed product properties", computedItem)
				priceElement.innerHTML = formatter.format(computedItem.price);
				if (computedItem.compareToPrice) {
					compareToPriceElement.innerHTML = formatter.format(computedItem.compareToPrice);
				} else {
					compareToPriceElement.innerHTML = '';
				}
				priceElement.classList.remove('pc__product__price--loading')
				compareToPriceElement.classList.remove('pc__product__comparetoprice--loading')
			}).catch((error) => {
				devLog("ERROR! Could not get price for this option", JSON.stringify(error.response))
			})
		}

		function changeOption(option, doAll) {
			currentCombination = false;
			// if (option.options) {
			// 	let selectedOption = option.options[option.selectedIndex];
			// } else {
			// 	let selectedOption = productElement.querySelector("[name='" + option.name + "']:checked");
			// }
			let currentValues = {};
			productElement.querySelectorAll('.pc__product__variations__option').forEach((productOption, j) => {
				if (productOption.type == 'radio') {
					currentValues[productOption.name] = productElement.querySelector("[name='" + productOption.name + "']:checked").value
				} else if (productOption.type == 'checkbox') {
					currentValues[productOption.name] = [];
					productElement.querySelectorAll("[name='" + productOption.name + "']:checked").forEach((item, i) => {
						currentValues[productOption.name].push(item.value);
					});
				} else {
					currentValues[productOption.name] = productOption.value;
				}
			});
			console.log(currentValues);
			if (productElement.dataset.combinations) {
				currentCombination = getCombination(currentValues, productElement.dataset.combinations);
			}
			if (doAll) {
				//  Check stock
				let productStock;
				if (thisProduct.unlimited) {
					productStock = 'unlimited';
				} else {
					productStock = parseInt(thisProduct.quantity);
				}
				let comboStock = false;
				if (currentCombination.unlimited) {
					comboStock = 'unlimited';
				} else if (currentCombination.quantity !== 'undefined' && currentCombination.quantity !== '') {
					if (!isNaN(parseInt(currentCombination.quantity))) {
						comboStock = parseInt(currentCombination.quantity)
					}
				}
				let finalStock;
				if (comboStock === false) {
					finalStock = productStock;
				} else {
					finalStock = comboStock;
				}

				// Update stock
				if (quantityOutput) {
					productElement.querySelectorAll('.pc__product__quantity__item').forEach((item, i) => {
						item.style.display = 'none';
					});
				}
				if (quantityInput) {
					quantityInput.disabled = false;
				}
				if (finalStock == 'unlimited') {
					if (quantityOutput) {
						productElement.querySelector('.pc__product__quantity__unlimited').style.display = '';
					}
					addToCartButton.disabled = false;
					if (quantityInput) {
						quantityInput.max = ''
					}
				} else if (finalStock == 0) {
					if (quantityOutput) {
						productElement.querySelector('.pc__product__quantity__out-of-stock').style.display = '';
					}
					addToCartButton.disabled = true;
					if (quantityInput) {
						quantityInput.disabled = true;
						quantityInput.max = 0;
					}
				} else if (finalStock >= 1) {
					if (quantityOutput) {
						productElement.querySelector('.pc__product__quantity__in-stock').style.display = '';
						productElement.querySelector('.pc__product__quantity__number').innerHTML = finalStock;
					}
					addToCartButton.disabled = false;
					if (quantityInput) {
						quantityInput.max = finalStock;
					}
				}

				// Update sku
				let extraValue = 0,
					extraValueCompareTo = 0;
				if (skuOutput) {
					if (currentCombination.id) {
						skuOutput.innerHTML = currentCombination.sku;
					} else {
						skuOutput.innerHTML = thisProduct.sku;
					}
				}

				// Update images
				let result, imageInfo = {}
				imageInfo['image_generator'] = "true";
				let comboImageObject = productElement.querySelector('.pc__product__media__combination');
				let imageObject = productElement.querySelector('.pc__product__media');
				if (currentCombination.image) {
					imageInfo['combo_id'] = currentCombination.id + '';
					async function request() {
						result = await xhrRequest('platecommerce', imageInfo, false);
					}
					request().then(function() {
						if (result.includes('NO-IMAGE-FOUND')) {
							comboImageObject.style.display = 'none';
							imageObject.style.display = '';
						} else {
							comboImageObject.innerHTML = result;
							comboImageObject.style.display = '';
							imageObject.style.display = 'none';
						}
					})
				} else {
					comboImageObject.style.display = 'none';
					imageObject.style.display = '';
				}
			}
			if (currentCombination) {
				productElement.dataset.variation_id = currentCombination.ecwidid;
			} else {
				productElement.dataset.variation_id = '';
			}

		}
	});
}

function getCombination(currentValues, inputJson) {
	let availableCombinations = JSON.parse(decodeURIComponent(inputJson));
	let currentCombination = false;
	let possibleCombination = false;
	let comparisonAmount = 0;
	availableCombinations.forEach((combination, k) => {
		let availableValues = {};
		combination.options.forEach((combinationOption, l) => {
			availableValues[combinationOption.naam] = combinationOption.value;
		});

		if (isEqual(currentValues, availableValues)) {
			currentCombination = combination;
		}
		if (Object.keys(currentValues).length > Object.keys(availableValues).length) {
			if (howMuchIsContainedIn(currentValues, availableValues) > comparisonAmount && howMuchIsContainedIn(currentValues, availableValues) > 0) {
				possibleCombination = combination;
			}
		}
	});
	if (currentCombination == false && possibleCombination) {
		currentCombination = possibleCombination
	}
	if (currentCombination) {
		devLog("Combination found", currentCombination);
	}
	return currentCombination;
}
const formatter = new Intl.NumberFormat('nl', {
	style: 'currency',
	currency: 'EUR',
	minimumFractionDigits: 2
})

function howMuchIsContainedIn(a, b) {
	let counter = 0;
	for (var property in a) {
		if (a.hasOwnProperty(property)) {
			if (a[property] == b[property]) {
				counter++;
			}
		}
	}
	return counter;
}


function isEqual(a, b) {
	let counter = 0;
	for (var property in a) {
		if (a.hasOwnProperty(property)) {
			if (a[property] == b[property]) {
				counter++;
			}
		}
	}
	if (counter == Object.keys(a).length) {
		return true;
	} else {
		return false;
	}
}



function isContainedIn(a, b) {
	if (typeof a != typeof b)
		return false;
	if (Array.isArray(a) && Array.isArray(b)) {
		// assuming same order at least
		for (var i = 0, j = 0, la = a.length, lb = b.length; i < la && j < lb; j++)
			if (isContainedIn(a[i], b[j]))
				i++;
		return i == la;
	} else if (Object(a) === a) {
		for (var p in a)
			if (!(p in b && isContainedIn(a[p], b[p])))
				return false;
		return true;
	} else
		return a === b;
}
export {
	pcProduct,
	getCombination
}