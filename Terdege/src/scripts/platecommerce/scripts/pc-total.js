import {
	PlateCommerce
} from './functions/PlateCommerce'
import {
	xhrRequest
} from './functions/request'
import {
	devLog
} from './functions/devlog'

let {
	Cart,
	OrderHandler,
	Config
} = PlateCommerce

async function pcTotal() {
	let totalInfo = {}
	let totals = document.querySelectorAll('.pc__total');
	totals.forEach((total, i) => {
		total.classList.add('pc__total--loading')
	});
	OrderHandler.computeOrderFromCart().then((computation) => {
		totalInfo['total_json'] = encodeURI(JSON.stringify(computation))
		// console.log({computation});
		let result;
		
		async function request() {
			result = await xhrRequest('platecommerce', totalInfo, false);
		}
		request().then(function() {
			totals.forEach((total, i) => {
				total.classList.remove('pc__total--loading')
				total.innerHTML = result;
			});
		})
	})
}

export {
	pcTotal
}