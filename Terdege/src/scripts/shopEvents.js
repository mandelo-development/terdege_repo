import {scrollCont} from './scrollContainer';

async function shopEvents() {

    document.addEventListener('productAdded', function(addEvent) {
        var btnEcwidID = addEvent.detail.id;
        var btnElement = document.querySelectorAll('.pc__product__add-to-cart[data-ecwid="' + btnEcwidID + '"]');
        console.log(btnElement)
        btnElement[0].classList.add("add_product--added");
        setTimeout(function () {
            btnElement[0].classList.remove("add_product--added");
        }, 2000)
	})
    document.addEventListener('cartReady', function(cartReadyEvent) {
        console.log(cartReadyEvent);
        setTimeout(function () {
            scrollCont.update();
            console.log('is updated')
        }, 100);
	})

} export {
    shopEvents
}