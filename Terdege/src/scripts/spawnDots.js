import {scrollCont} from './scrollContainer';

async function spawnDots() {
    var $ = require('../../../config/node_modules/jquery');

    var container = $("#dots-container");
    if(container.length > 0) {
        var times = Math.floor(Math.random() * (10 - 7 + 1) + 7);
        var colors = ["#85AB90", "#ED6862", "#D98C0C", "#F5A488"];

        for (let t = 0; t < times; t++) {
            var randomSpeed = Math.floor(Math.random()*5) + 1;
            var randomColor = colors[Math.floor(Math.random()*colors.length)];

            $(container).append(
                $("<div/>", {
                    "class": "dot-wrapper",
                    "data-scroll": "",
                    "data-scroll-direction": "vertical",
                    "data-scroll-speed": randomSpeed,

                    append: $('<div/>', {
                        "class": "dot dot-" + t + "",
                    }).css({
                        backgroundColor: randomColor,
                    })
                }).css({
                    left: Math.random() * ($(container).width() - 20),
                    top: Math.random() * ($(container).height() - 20),
                })
            );

            if(t === times - 1) {
                scrollCont.update();
            }
        }
    }

} export {
    spawnDots
}