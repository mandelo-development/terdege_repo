// Import Swiper and modules
import {
	Swiper,
    Navigation,
    Pagination,
    Scrollbar,
    Controller,
	Lazy,
	Thumbs
} from '../../../config/node_modules/swiper/js/swiper.esm.js';
Swiper.use([Navigation, Pagination, Scrollbar, Controller, Lazy, Thumbs]);

async function swiperInstances() { 
	var $ = require('../../../config/node_modules/jquery');

	$(document).ready(function () {
		
		if ($('.slider-container.aanbevolen-evenementen').length) {
			var event_slider = new Swiper('.slider-container.aanbevolen-evenementen', {
				init: false,
				autoplay: false,
				direction: 'horizontal',
				grabCursor: true,
				loop: false,
				navigation: false,
				breakpoints: {
					0: {
						slidesPerView: 1.6,
						spaceBetween: 30,
					},
					1366: {
						slidesPerView: 1.8,
						spaceBetween: 70,
					}
				},
				mousewheel: {
					forceToAxis: true,
				}
			});
		
			event_slider.init();
			event_slider.snapGrid[event_slider.snapGrid.length - 1] = event_slider.slidesGrid[event_slider.slidesGrid.length - 1];
		}
		
		var media_slider = new Swiper('.slider-container.media', {
			autoplay: false,
			spaceBetween: 26,
			slidesPerView: 1,
			direction: 'horizontal',
			grabCursor: true,
			loop: false,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			mousewheel: {
				forceToAxis: true,
			}
		});

		var product_media_thumbs = new Swiper('.media-gallery-thumbs', {
			spaceBetween: 20,
			slidesPerView: 'auto',
			freeMode: false,
			grabCursor: false,
			watchSlidesVisibility: true,
			watchSlidesProgress: true,
			allowTouchMove: false,
		});	
		var product_media_slider = new Swiper('.media-gallery-top', {
			spaceBetween: 26,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			thumbs: {
				swiper: product_media_thumbs
			},
			mousewheel: {
				forceToAxis: true,
			}
		});


	});

 } export {
	 swiperInstances
 }